public class Sample{

	// display an array of Complex numbers to standard output
    public static void show(Complex[] x, String title) {
        System.out.println(title);
        System.out.println("-------------------");
        for (int i = 0; i < x.length; i++) {
            System.out.println(x[i]);
        }
        System.out.println();
    }

	public static void main(String[] args){
		
		int N = Integer.parseInt(args[0]);
        Complex[] x = new Complex[N];

        // original data
        for (int i = 0; i < N; i++) {
            x[i] = new Complex(i, 0);
            x[i] = new Complex(-2*Math.random() + 1, 0);
        }
        
        show(x, "x");

        // FFT of original data
        Complex[] y = FFT.fft(x);
        show(y, "y = fft(x)");

        // take inverse FFT
        Complex[] z = FFT.ifft(y);
        show(z, "z = ifft(y)");

	}
}