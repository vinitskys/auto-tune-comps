import wave

# creates a new version of fileNamePrefix.wav with the first N seconds removes
def removeFirstNSeconds(fileNamePrefix, N):

	w = wave.open(fileNamePrefix + ".wav", "rb")
	v = wave.open(fileNamePrefix + "Removed" + str(N) + ".wav","wb")

	# (nchannels, sampwidth, framerate, nframes, comptype, compname)
	v.setparams(w.getparams())

	w.setpos(int(w.getframerate()) * N)

	for i in range(w.getnframes()):
		v.writeframes(w.readframes(1))

# creates a new version of fileNamePrefix with each frame repeated N times
def repeatEachFrameNTimes(fileNamePrefix, N):

	w = wave.open(fileNamePrefix + ".wav", "rb")
	v = wave.open(fileNamePrefix + "Repeated" + str(N) + ".wav","wb")

	# (nchannels, sampwidth, framerate, nframes, comptype, compname)
	v.setparams(w.getparams())

	for i in range(w.getnframes()):
		frameToWrite = w.readframes(1) 
		for j in range(N):
			v.writeframes(frameToWrite)


fileNamePrefix = "walken"

removeFirstNSeconds(fileNamePrefix, 5)
removeFirstNSeconds(fileNamePrefix, 10)
removeFirstNSeconds(fileNamePrefix, 15)

repeatEachFrameNTimes(fileNamePrefix, 2)
repeatEachFrameNTimes(fileNamePrefix, 4)