import java.io.*;

public class Offset
{
	public static void main(String[] args)
	{
		try
		{
			// Open the wav file specified as the first argument
			WavFile inWav = WavFile.openWavFile(new File(args[0]));
			int newNumFrames = (int)inWav.getNumFrames() - Integer.parseInt(args[2]) * (int)inWav.getSampleRate();
			WavFile outWav = WavFile.newWavFile(new File(args[1]), inWav.getNumChannels(), newNumFrames, inWav.getValidBits(), inWav.getSampleRate());

            
            int framesRead = 0;
            int[] buffer = new int[(int)inWav.getNumFrames()];
            
			inWav.readFrames(buffer, buffer.length);
			
		    int[] bufferOut = new int[buffer.length - Integer.parseInt(args[2]) * (int)outWav.getSampleRate()];
		    
		    int j = 0;
		    for(int i = Integer.parseInt(args[2]) * (int)outWav.getSampleRate(); i < buffer.length; i++){
		        bufferOut[j] = buffer[i];
		        j++;
		    }
		    
		    outWav.writeFrames(bufferOut, bufferOut.length);
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}
}
