import java.io.*;

public class DoubleSpeed
{
	public static void main(String[] args)
	{
		try
		{
			// Open the wav file specified as the first argument
			WavFile inWav = WavFile.openWavFile(new File(args[0]));

			WavFile outWav = WavFile.newWavFile(new File(args[1]), inWav.getNumChannels(), inWav.getNumFrames() * 2, inWav.getValidBits(), inWav.getSampleRate());
            int framesRead = 0;
            double[] buffer = new double[100 * inWav.getNumChannels()];
            int count = 0;
            do{
                framesRead = inWav.readFrames(buffer, 100);
                for(int i = 0; i < framesRead; i++){
                    double[] temp = new double[2];
                    temp[0] = buffer[i];
                    temp[1] = buffer[i];
                    System.out.println("here");
                    outWav.writeFrames(temp, count, 2);
                    System.out.println("2");
                    count += 2;
                }
            } while (framesRead != 0);
            inWav.close();
            outWav.close();
            
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}
}