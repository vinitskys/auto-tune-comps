import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class Main {
	public static void main(String[] args) {
		int CHUNK_SIZE = 32;
		String fn = "clav.wav";

		try {
			InputStream stream = new BufferedInputStream(new FileInputStream(fn));
			AudioInputStream src = AudioSystem.getAudioInputStream(stream);

			int FRAME_SIZE = src.getFormat().getFrameSize();
			double NORM_FACTOR = Math.pow(2, -8*FRAME_SIZE+1);
			
			byte[] buffer = new byte[CHUNK_SIZE*FRAME_SIZE];
			src.read(buffer);

			Complex[] chunk = new Complex[CHUNK_SIZE];
			for (int i = 0; i < CHUNK_SIZE; i++) {
				short sample = (short) (((buffer[2*i+1] & 0xff) << 8) | (buffer[2*i] & 0xff));
				chunk[i] = new Complex(NORM_FACTOR * sample, 0);
			}

			System.out.println();
			for (int i = 0; i < CHUNK_SIZE; i++)			
				System.out.println(chunk[i]);

			FFT.fft(chunk);
			//FFT.ifft(chunk);

			System.out.println();
			for (int i = 0; i < CHUNK_SIZE; i++)			
				System.out.println(chunk[i]);

		} catch (Exception e) {
			System.out.println("Oops, something catastrophic happened...");
			System.out.println(e);
		}
	}

}