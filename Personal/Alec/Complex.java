import java.util.Formatter;

public class Complex {
	public double real, imag;
	public Complex() {
		this(0, 0);
	}
	public Complex(double real, double imag) {
		this.real = real;
		this.imag = imag;
	}
	public String toString() {
		return real + ((imag >= 0) ? "+" : "") + imag + "i";
	}
	public Complex clone() {
		return new Complex(real, imag);
	}
	public void conj() {
		imag *= -1;
	}
}