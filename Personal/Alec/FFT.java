

public class FFT {

	private static int brev(int x, int n) {
		int y = 0;
		while (n-- > 0) {
			y <<= 1;
			y |= (x & 1);
			x >>= 1;
		}
		return y;
	}

	public static Complex[] fft(Complex[] data) {
		int size = data.length;
		int base = (int) Math.round(Math.log(size) / Math.log(2));
		if (Math.pow(2, base) != size) {
			throw new RuntimeException("FFT size not a power of 2!");
		}
		double unf = Math.pow(size, -0.5);

		// butterfly swap
		for (int x = 0; x < size; x++) {
			int y = brev(x, base);
			if (x < y) {
				Complex temp = data[x];
				data[x] = data[y];
				data[y] = temp;
			}
			data[x].real *= unf;
			data[x].imag *= unf;
		}

		int d = 1; // step between each sub-iteration
		double f = Math.PI; // twiddle angle (CHANGE TO DENOMINATOR, size/2 WHEN CONVERTING TO TABLE LOOKUP)
		while (d < size) {
			int i = 0; // master index, current position inside full array
			while (i < size) {
				int k = 0; // relative offset within current sub-iteration
				while (k < d) {
					int j = i + d;

					double tw_cos = Math.cos(f*k);
					double tw_sin = Math.sin(f*k);

					Complex a = data[i].clone();
					Complex b = new Complex();
					b.real = data[j].real*tw_cos + data[j].imag*tw_sin;
					b.imag = data[j].imag*tw_cos - data[j].real*tw_sin;

					data[i].real = a.real + b.real;
					data[i].imag = a.imag + b.imag;
					data[j].real = a.real - b.real;
					data[j].imag = a.imag - b.imag;

					k++; // step ahead in the sub-iteration
					i++; // update master index to reflect this
				}
				i += d; // move master index ahead to next sub-iteration
			}
			// bump up to next step size
			d <<= 1;
			f /= 2;
		}

		return data;
	}

	public static Complex[] ifft(Complex[] data) {
		for (int i = 0; i < data.length; i++)
			data[i].conj();
		data = fft(data);
		for (int i = 0; i < data.length; i++)
			data[i].conj();
		return data;
	}

	public static void main(String[] args) {
		int n = 1024;
		Complex[] data = new Complex[n];
		for (int i = 0; i < n; i++) {
			data[i] = new Complex(i+0.5, -i/2.0);
		}

		//System.out.println(java.util.Arrays.toString(data));
		//System.out.println(java.util.Arrays.toString(ifft(fft(data))));
		for (int i = 0; i < 1000; i++)
			ifft(fft(data));
	}
}
