import sys
import wave, pyaudio
import struct

def main():
	BUFFER_SIZE = 256

	src = wave.open(sys.argv[1], 'rb')
	pa = pyaudio.PyAudio()

	width = src.getsampwidth()
	nchan = src.getnchannels()

	# Open stream
	stream = pa.open(
		format = pa.get_format_from_width(width),
		channels = nchan,
		rate = src.getframerate(),
		output = True
	)

	framesize = nchan*width

	# Read until we can't anymore
	while True:
		pcm = src.readframes(BUFFER_SIZE)
		if not pcm:
			break
		pcm2 = ""
		for i in xrange(BUFFER_SIZE):
			frame = pcm[framesize*i:framesize*(i+1)]
			pcm2 += frame + frame

		stream.write(pcm2)

	# clean up
	stream.stop_stream()
	stream.close()
	pa.terminate()


if __name__ == '__main__':
	main()
