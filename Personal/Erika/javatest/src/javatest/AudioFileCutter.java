package javatest;
import java.io.*;
import javax.sound.sampled.*;


public class AudioFileCutter {

	//4 args - input file, output file, bytes to cut from beginning, bytes to cut from end
	public static void main(String[] args) {
		AudioInputStream music = null;
		try {
			File file = new File(args[0]);
			AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(file);
			music = AudioSystem.getAudioInputStream(file);
		    music.skip((Long.parseLong(args[2])));
		    music = new AudioInputStream(music, fileFormat.getFormat(), music.getFrameLength() - (Long.parseLong(args[2])+Long.parseLong(args[3]))/(fileFormat.getFormat().getFrameSize()));
		    AudioSystem.write(music, fileFormat.getType(), new File(args[1]));
		} catch (Exception e){
			System.out.println(e);
		}

	}

}
