package fft;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

public class TestFFT {
	public static void main(String[] args) {
		String filename = "A_440_Hz.wav";
		try{
			AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(new File(filename));
			AudioInputStream music = AudioSystem.getAudioInputStream(new File(filename));
		    AudioFormat format=music.getFormat();
		    System.out.println(format.getSampleRate()+" "+format.getSampleSizeInBits()+" "+format.getChannels()+" "+format.isBigEndian());
				
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		WAV input = new WAV(filename);
		byte[] inputArray = input.wavToByteArray();
		byte[] output = new byte[inputArray.length];
		System.out.println("LENGTH"+inputArray.length);
		for(int i=0; i<inputArray.length; i+=32768){
			byte[] tempInput = new byte[32768];
			for(int j=0; j<tempInput.length;j++){
				tempInput[j] = inputArray[i+j];
			}
			byte[] tempOutput = calculateFFT(tempInput);
			for(int j=0; j<tempInput.length;j++){
				output[i+j] = tempOutput[j];
			}
		}
		try{
        	PrintWriter writer = new PrintWriter("fftoutput.txt", "UTF-8");
	        for(int i = 0; i < 32768; i++){
	        	writer.println(inputArray[i+32768*8]+", "+output[i+32768*8]);
	        }
	        writer.close();
        } catch(Exception e){
        	System.out.println(e.getMessage());
        }
		
		WAV reconstructed = new WAV(output, "440.wav");
	}
	
	public static byte[] calculateFFT(byte[] signal)
    {           
        //final int mNumberOfFFTPoints =signal.length/2-1;
        final int mNumberOfFFTPoints = 32768;
        double mMaxFFTSample;

        double temp;
        Complex[] y = null;
        Complex[] complexSignal = new Complex[mNumberOfFFTPoints];
        double[] absSignal = new double[mNumberOfFFTPoints/2];
        try{
        	PrintWriter writer = new PrintWriter("fftoutput.txt", "UTF-8");
	        for(int i = 0; i < mNumberOfFFTPoints; i++){
	            temp = (double) signal[i]/32768.0;
	        	complexSignal[i] = new Complex(temp,0.0);
	        }
	        y = FFT.fft(complexSignal); // --> Here I use FFT class
	        for(int i=0; i<y.length; i++){
	        	writer.println(complexSignal[i].toString());
	        }
	        writer.close();
        } catch(Exception e){
        	System.out.println(e.getMessage());
        }
        
        mMaxFFTSample = 0.0;
        int mPeakPos = 0;
        for(int i = 0; i < (mNumberOfFFTPoints/2); i++)
        { 
             absSignal[i] = Math.sqrt(Math.pow(y[i].re(), 2) + Math.pow(y[i].im(), 2));
             if(absSignal[i]>10){
            	 System.out.println(""+absSignal[i]);
                 System.out.println(""+i*2.69);
             } 
             if(absSignal[i] > mMaxFFTSample)
             { 
                 mMaxFFTSample = absSignal[i];
                 mPeakPos = i;
             } 
        }        
        
        Complex[] ifftOutput = FFT.ifft(y);
        byte[] toReturn = new byte[mNumberOfFFTPoints*8];
        for(int i =0; i < mNumberOfFFTPoints; i++){

        	toReturn[i] = (byte)(int)(ifftOutput[i].re()*32768.0);
        }
        
        return toReturn;
    }
	
}
