package fft;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;


public class WAV{
	String filename;

	public WAV(String wavFilename){
		filename = wavFilename;
	}
	
	
	public WAV(byte[] ifftOutput, String fn) {
		filename = fn;

		AudioFormat frmt = new AudioFormat((float)44100.0, 16, 2, true, false);
		AudioInputStream ais = new AudioInputStream(new ByteArrayInputStream(ifftOutput), frmt, ifftOutput.length / frmt.getFrameSize());
		  
		  try {
		    AudioSystem.write(ais, AudioFileFormat.Type.WAVE, new File(filename));
		  } 
		  catch(Exception e) {
		    e.printStackTrace();
		  }
	}


	public byte[] wavToByteArray(){
		byte[] audioBytes = new byte[0];
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try{
			BufferedInputStream bufferedIn = new BufferedInputStream(new FileInputStream(filename));
			AudioInputStream in = AudioSystem.getAudioInputStream(bufferedIn);
	
			int read;
			byte[] buff = new byte[1024];
			while ((read = in.read(buff)) > 0)
			{
			    out.write(buff, 0, read);
			}
			out.flush();
			audioBytes = out.toByteArray();
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		return audioBytes;
	}
	
	
}

