package autotune;
/**
 * A thread that detects pitch so that source and target files can by analyzed in parallel.
 */
public class PitchThread extends Thread{
	private PitchDetector p;
	private SignalSpectrum s;
	public double pitch;
	
	public PitchThread(SignalSpectrum signal, PitchDetector p){
		s = signal;
		this.p = p;
	}
	
	public void run(){
		this.pitch = p.pitchDetect(s);
	}
}
