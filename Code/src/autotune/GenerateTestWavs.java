package autotune;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

public class GenerateTestWavs {
		
	public static void main(String[] args){
//		byte[] wave = simpleSine(440);
		byte[] wave = decreasingAmplitudeHarmonics(400);
		for(int i = 0; i < wave.length / 100; i++){
			System.out.println(i+ "\t" + wave[i]);
		}
		if(args.length == 0){
			writeToWavFile(wave, "wavs/test.wav");
		}
		else{
			writeToWavFile(wave, args[0]);
		}
	}
	
	public static byte[] melody(){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE * 2 * 3.5)];
		byte[] f = logDecAmpHarmonics(43, 5, .8);
		System.out.println((AutoTune.SAMPLE_RATE * 2 * 4));
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 2 * 2 / 5; i++){
			wave[i] = f[i];
		}
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 2 / 10; i++){
			wave[(int) (i + AutoTune.SAMPLE_RATE * 2 * 2 / 5)] = 0;
		}
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 2 * 2 / 5; i++){
			wave[(int) (i + AutoTune.SAMPLE_RATE * 2 / 2)] = f[i];
		}
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 2 / 10; i++){
			wave[(int) (i + AutoTune.SAMPLE_RATE * 2 * 4 / 5)] = 0;
		}
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 2 * 2 / 5; i++){
			wave[(int) (i + AutoTune.SAMPLE_RATE * 2)] = f[i];
		}
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 2 / 10; i++){
			wave[(int) (i + AutoTune.SAMPLE_RATE * 2 * 7 / 5)] = 0;
		}
		byte[] db = logDecAmpHarmonics(34, 5, .6);
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 3 / 5; i++){
			wave[(int) ((int) (i + AutoTune.SAMPLE_RATE * 2 * 7 / 5) + AutoTune.SAMPLE_RATE * 2 / 10)] = db[i];
		}
		byte[] ab = logDecAmpHarmonics(51, 5, .2);
		for(int i = 0; i < AutoTune.SAMPLE_RATE * 1 / 5; i++){
			wave[(int) ((int) ((int) (i + AutoTune.SAMPLE_RATE * 2 * 7 / 5) + AutoTune.SAMPLE_RATE * 2 / 10) + AutoTune.SAMPLE_RATE * 3 / 5)] = ab[i];
		}
		for(int i = (int) ((int) ((int) ((int) (AutoTune.SAMPLE_RATE * 2 * 7 / 5) + AutoTune.SAMPLE_RATE * 2 / 10) + AutoTune.SAMPLE_RATE * 3 / 5) + AutoTune.SAMPLE_RATE * 1 / 5); i < wave.length; i++){
			wave[i] = wave[i - ((int) (AutoTune.SAMPLE_RATE * 2 * 5 / 5))];
		}
		return wave;
	}
	/*
	 * I'm not sure why, but it outputs a really ugly noise if wave2 is 44100 or longer. 44099 is just fine.
	 */
	public static byte[] concatenateWaves(byte[] wave1, byte[] wave2){
		byte[] outWave = new byte[wave1.length + 44099];
		for(int i = 0; i < wave1.length; i++){
			outWave[i] = wave1[i];
		}
		for(int i = wave1.length; i < wave1.length + wave2.length - 100; i++){
			outWave[i - 1] = wave2[i - wave1.length];
		}
		return outWave;
	}
	
	/*
	 * add together two waves
	 */
	private static byte[] addWaves(byte[] wav1, byte[] wav2){
		assert(wav1.length == wav2.length);
		byte[] outWav = new byte[wav1.length];
		for(int i = 0; i < wav1.length; i++){
			outWav[i] = (byte) ((wav1[i] + wav2[i]) /2);
		}
		return outWav;
	}
	/*
	 * amplitude of harmonics decreases with the log of the harmonic number, resembles real noise.
	 */
	public static byte[] logDecAmpHarmonics(double freq){
		return logDecAmpHarmonics(freq, 5);
	}
	public static byte[] logDecAmpHarmonics(double freq, int numHarmonics){
		return logDecAmpHarmonics(freq, numHarmonics, 1);
	}
	public static byte[] logDecAmpHarmonics(double freq, int numHarmonics, double secs){
		byte[] wave = new byte[(int) ((int) (AutoTune.SAMPLE_RATE) * secs)];
		
		for(int i = 0; i < AutoTune.SAMPLE_RATE * secs; i++){
			for(int j = 0; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 127 / Math.log(j + 2) / numHarmonics);
			}
		}
		
		return wave;
	}
	
	private static void writeToWavFile(byte[] signal, String file){
		AudioFormat fmat = AutoTune.REQUIRED_AUDIO_FORMAT;
		AudioInputStream ais = new AudioInputStream(new ByteArrayInputStream(signal), fmat, signal.length/fmat.getFrameSize());
		try{
			AudioSystem.write(ais, AudioFileFormat.Type.WAVE, new File(file));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	/*
	 * Generate a sine wave with a length of one second and a given frequency
	 * Does not contain any harmonics or other irregularities
	 * 
	 * @param freq 	frequency of the sine wave, same as the pitch
	 * @return 		an array of doubles representing the signal in the time domain
	 */
	public static byte[] simpleSine(double freq){
		return simpleSine(freq, 1);
	}
	
	/*
	 * @param freq 	frequency of the sine wave
	 * @param secs 	Number of seconds the wave will be
	 * @return 		an array of bytes containing the signal
	 */
	public static byte[] simpleSine(double freq, double secs){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE * secs)];
		for(int i = 0; i < AutoTune.SAMPLE_RATE * secs; i++){
			double angle = i / (AutoTune.SAMPLE_RATE / freq) * Math.PI;
			wave[i] = (byte)(Math.sin(angle) * 127.0);
		}
		return wave;
	}
	
	/*
	 * Create a wave containing constant amplitude harmonics based off of the given frequency.
	 * 10 harmonics are added by default
	 * 
	 * @param freq	fundamental frequency of the desired wave
	 * @return 		an array of bytes containing the wave
	 */
	public static byte[] constantAmplitudeHarmonics(double freq){
		return constantAmplitudeHarmonics(freq, 10, 1);
	}
	
	
	/*
	 * Create a wave containing constant amplitude harmonics based off of the given frequency.
	 *  
	 * @param freq			fundamental frequency of the desired wave
	 * @param numHarmonics	the number of harmonics present in the wave
	 * @return 				an array of bytes containing the wave
	 */
	public static byte[] constantAmplitudeHarmonics(double freq, int numHarmonics){
		return constantAmplitudeHarmonics(freq, numHarmonics, 1);
	}
	
	/*
	 * Create a wave containing constant amplitude harmonics based off of the given frequency.
	 *  
	 * @param freq			fundamental frequency of the desired wave
	 * @param numHarmonics	the number of harmonics present in the wave
	 * @param secs			number of seconds the wave will last
	 * @return 				an array of bytes containing the wave
	 */
	public static byte[] constantAmplitudeHarmonics(double freq, int numHarmonics, int secs){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE * secs)];
		
		for(int i = 0; i < AutoTune.SAMPLE_RATE * secs; i++){
			for(int j = 0; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 127.0 / numHarmonics);
			}
		}
		
		return wave;
	}
	
	public static byte[] decreasingAmplitudeHarmonics(double freq){
		return decreasingAmplitudeHarmonics(freq, 10, 1);
	}
	public static byte[] decreasingAmplitudeHarmonics(double freq, int numHarmonics){
		return decreasingAmplitudeHarmonics(freq, numHarmonics, 1);
	}
	/*
	 * Create a wave containing decreasing amplitude harmonics based off of the given frequency.
	 * As you get farther away from the fundamental frequency, the amplitude of the harmonic decreases.
	 *  
	 * @param freq			fundamental frequency of the desired wave
	 * @param numHarmonics	the number of harmonics present in the wave
	 * @param secs			number of seconds the wave will last
	 * @return 				an array of bytes containing the wave
	 */
	public static byte[] decreasingAmplitudeHarmonics(double freq, int numHarmonics, int secs){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE) * secs];
		
		for(int i = 0; i < AutoTune.SAMPLE_RATE * secs; i++){
			for(int j = 0; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 63 / Math.pow(2, j));
			}
		}
		
		return wave;
	}

	/*
	 * missing the fundamental frequency, with constant amplitude harmonics
	 */
	public static byte[] missingFundamentalConstantHarmonics(int freq){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE)];
		int numHarmonics = 10;
		
		for(int i = 0; i < AutoTune.SAMPLE_RATE; i++){
			for(int j = 1; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 63.0 / numHarmonics);
			}
		}

		return wave;
	}
	
	/*
	 * missing the fundamental frequency, with harmonics that are decreasing in amplitude.
	 */
	public static byte[] missingFundamentalDecreasingHarmonics(int freq){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE)];
		int numHarmonics = 10;
		
		for(int i = 0; i < AutoTune.SAMPLE_RATE; i++){
			for(int j = 1; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 127.0 / Math.pow(2, j));
			}
		}

		return wave;
	}
	
	/*
	 * freq will be the tallest peak, hopefully
	 */
	public static byte[] undertonesAndFundamental(int freq){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE)];
		int numHarmonics = 5;
		
		for(int i = 0; i < AutoTune.SAMPLE_RATE; i++){
			for(int j = 0; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 127.0 / Math.pow(2, j) / 2);
			}
			for(int j = 1; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (freq / i) ) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 63.0 / Math.pow(2, j) / 2);
			}
		}

		return wave;
	}
	
	/*
	 * This is silly, don't use this.
	 */
	public static byte[] undertonesLoudest(int freq){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE)];
		int numHarmonics = 2;
		
		for(int i = 0; i < AutoTune.SAMPLE_RATE; i++){
			for(int j = -numHarmonics; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * 63.0 / Math.pow(2, j + numHarmonics + 1));
			}
		}

		return wave;
	}
	
	public static byte[] randomAmpHarmonics(int freq){
		return randomAmpHarmonics(freq, 5);
	}
	
	public static byte[] randomAmpHarmonics(int freq, int numHarm){
		return randomAmpHarmonics(freq, numHarm, 1);
	}
	
	public static byte[] randomAmpHarmonics(int freq, int numHarm, double secs){
		byte[] wave = new byte[(int) (AutoTune.SAMPLE_RATE)];
		int numHarmonics = numHarm;
		int[] amps = new int[numHarm + 1];
		for(int i = 0; i < amps.length; i++){
			amps[i] = (int) (Math.random() * 127.0/numHarmonics);
			System.out.println("Freq: " + (i + 1) * freq + "\tAmp: " + amps[i]);
		}
		for(int i = 0; i < AutoTune.SAMPLE_RATE; i++){
			for(int j = 1; j < numHarmonics; j++){
				double angle = i / (AutoTune.SAMPLE_RATE / (j * freq)) * Math.PI;
				wave[i] += (byte)(Math.sin(angle) * amps[j - 1]);
			}
		}

		return wave;
	}
}
