package autotune;

import org.kohsuke.args4j.*;
/**
 * A class to hold the main method. <p>
 * Processes command line options and begins autotuning
 */
public class AutoTuneMain {
	
	/**
	 * @param args command line arguments
	 * @see Options
	 * @see AutoTune
	 */
	public static void main(String[] args){
		Options opt = new Options();
		CmdLineParser parser = new CmdLineParser(opt);
        try {
        	parser.parseArgument(args);
        	AutoTune.run(opt);
        } catch (CmdLineException e) {
            // handling of wrong arguments
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
        }
	}
}
