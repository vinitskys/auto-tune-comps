package autotune;

import java.util.HashSet;
import java.util.ArrayList;

import javax.sound.midi.*;

/**
 * To use:
 *  - initialize via new MidiListener()
 *  - will throw MidiUnavailableException if no devices can be opened
 *  - call getPitch() to query the keyboard's current pitch; returns 0 if no key is depressed
 *  - MAKE SURE TO CALL close()
 */
public class MidiListener implements Receiver {
	protected double lastPitch;
	private MidiDevice inputDevice;
	private HashSet<Integer> notesPressed;
	private static double pitchBend = 0;
	
	private static final int MAX_POLYPHONY = 10; // if we go over 10 notes pressed, I will be very sad. It slows it down too much anyways.
	private static final int MIDI_A440 = 69;

	
	
	/**
	 * @throws MidiUnavailableException we know this will happen during our presentation
	 */
	public MidiListener() throws MidiUnavailableException {
		lastPitch = 0;
		Transmitter inputDevice = MidiSystem.getTransmitter();
		inputDevice.setReceiver(this);
		notesPressed = new HashSet<Integer>();
	}
	
	/**
	 * used internally to convert key numbering to pitch
	 * @param note the integer value of the note from the MIDI
	 * @return double the hertz value of the note
	 */
	private static double getPitchFromMidiNote(int note) {
		return 440 * Math.pow(2, (note + pitchBend - MIDI_A440) / 12f);
	}

	/**
	 * called automatically when the JVM receives a MIDI message from the input device(non-Javadoc)
	 * @param midiMessage it's a midi Message
	 * @param timeStamp the time it is sent
	 * @see javax.sound.midi.Receiver#send(javax.sound.midi.MidiMessage, long)
	 */
	public void send(MidiMessage midiMessage, long timeStamp) {
		if (!(midiMessage instanceof ShortMessage))
			return;
		
		ShortMessage msg = (ShortMessage) midiMessage;
		if(AutoTune.debug){
			System.out.println(msg.getCommand());
			System.out.println("MSG 1, MIDI: " + msg.getData1());
			System.out.println("MSG 2, MIDI: " + msg.getData2());
		}

		switch (msg.getCommand()) {
		case ShortMessage.PITCH_BEND:
			pitchBend = msg.getData2()/64.0 - 0.5; // only MSB (for now?)
			break;
		case ShortMessage.NOTE_ON:
			if (msg.getData2() == 0)
				notesPressed.remove(msg.getData1());
			else if (notesPressed.size() <= MAX_POLYPHONY)
				notesPressed.add(msg.getData1());
			break;
		case ShortMessage.NOTE_OFF:
			notesPressed.remove(msg.getData1());
			break;
		}
	}
	
	/**
	 * Return all of the pitches currently being played on MIDI device
	 * @return A queue of pitches currently pressed
	 */
	public ArrayList<Double> getPitches() {
		ArrayList<Double> pitches = new ArrayList<Double>();
		for(Integer note : notesPressed)
			pitches.add(getPitchFromMidiNote(note));

		return pitches;
		
	}
	/**
	 * must be called explicitly; input device won't get freed automatically upon JVM termination so this is important!!!(non-Javadoc)
	 * @see javax.sound.midi.Receiver#close()
	 */
	public void close() {
		inputDevice.close();
	}
}
