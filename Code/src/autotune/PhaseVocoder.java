

/*
 * - source file with only one bin (bin64)
 * - figure out which beta to use to get it between two bins (bin# / 64)
 * - see what happens
 */

package autotune;
/** 
 *  PhaseVocoder.java 
 *  
 *  
 *  ORDER: (see Dolson 2 for descriptions of each)
 *  1. Peak detection
 *  2. Calculate frequency shifts
 *  3. Shift peaks
 *  4. Adjust phases
 *  
 */
class PhaseVocoder {
	
	protected static final double VOI_LOWER_HZ =    0.0;
	protected static final double VUV_XOVER_HZ = 10000.0;
	protected static final double UNV_UPPER_HZ = 15000.0;
	
	protected static final int VOI_LOWER_BIN = (int) Math.ceil (VOI_LOWER_HZ / AutoTune.SAMPLE_RATE * AutoTune.WINDOW_SIZE);
	protected static final int VUV_XOVER_BIN = (int) Math.round(VUV_XOVER_HZ / AutoTune.SAMPLE_RATE * AutoTune.WINDOW_SIZE);
	protected static final int UNV_UPPER_BIN = (int) Math.floor(UNV_UPPER_HZ / AutoTune.SAMPLE_RATE * AutoTune.WINDOW_SIZE);

	// Accumulates the phase adjustments from one frame to the next
	private static double[] phaseAdjusts = new double[AutoTune.WINDOW_SIZE/2+1];
	
	public static double time = 0;
	private static double start;
	
	// Given processed real and imag parts, mirrors around the Nyquist freq and
	// returns the iFFT'd signal
	private static double[] reconstruct(double[] real, double[] imag) {
		assert (real.length == imag.length);
		for (int i = 1; i < real.length/2; i++) {
			real[real.length-i] =  real[i];
			imag[imag.length-i] = -imag[i];
		}
		FFT.ifft(real, imag);
		time += System.currentTimeMillis() - start;
		return real;
	}	
	
	public static double[] phaseVocode(SignalSpectrum signal, double beta) {
		if (AutoTune.debug)
			System.out.println("beta = " + beta);
		
		start = System.currentTimeMillis();
		signal.detectPeaks();
		signal.interpolatePeaks();
		signal.findAreasOfInfluence();
		signal.estimateEnvelope();
		
		double[] real = signal.real.clone();
		double[] imag = signal.imag.clone();
		
		double[] shiftedReal = new double[real.length];
		double[] shiftedImag = new double[imag.length];	
		
		// process each area of influence (corresponds to exactly one spectrum peak)
		for (int peakIndex = 0; peakIndex < signal.peakBins.size(); peakIndex++) {
			int peakBin = signal.peakBins.get(peakIndex);
			double change = Math.round ((beta - 1) * peakBin);
			
			// copy sub and unv freqs unaltered
			for (int i = 0; i < VOI_LOWER_BIN; i++) {
				shiftedReal[i] = real[i];
				shiftedImag[i] = imag[i];
			}
			for (int i = UNV_UPPER_BIN; i <= signal.length/2; i++) {
				shiftedReal[i] = real[i];
				shiftedImag[i] = imag[i];
			}
			
			// translate each bin within the given area of influence
			int lowerBoundary = signal.aoi.get(peakIndex);
			int upperBoundary = signal.aoi.get(peakIndex+1);
			for (int binIndex = lowerBoundary; binIndex < upperBoundary; binIndex++) {
			//int binIndex = peakBin;
				
				// accumulate phase adjustments (keep things on [-pi, pi] to avoid overflow)
				phaseAdjusts[binIndex] = rng(phaseAdjusts[binIndex] + 2*Math.PI*change*AutoTune.HOP_FRACTION);
				double destPhase = signal.args[binIndex] + phaseAdjusts[binIndex];

				int destBin = binIndex + (int) change;				
				if (destBin >= signal.length/2 || destBin < 0) continue;
				
				double env = 1;//Math.sqrt(signal.envelope[destBin] / signal.mods[peakBin]);
				
				shiftedReal[destBin] += env * signal.mods[binIndex] * Math.cos(destPhase);
				shiftedImag[destBin] += env * signal.mods[binIndex] * Math.sin(destPhase);
			}
		}
		
		return reconstruct(shiftedReal, shiftedImag);
	}
	
	// restricts the given angle x to the range [-pi, pi]
	// (yes, making the if conditions line up vertically was more important than making one of the endpoints open)
	private static double rng(double x) {
		x = (x / Math.PI) % 2;
		if (x < 1) x += 2;
		if (x > 1) x -= 2;
		return Math.PI*x;
	}
	
}
