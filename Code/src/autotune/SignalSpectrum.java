package autotune;

import java.util.ArrayList;
/**
 * A class which contains all the useful information for a signal
 *
 */
class SignalSpectrum {
	protected int length;
	protected double[] signal;
	protected double[] real;
	protected double[] imag;
	protected double[] mods;
	protected double[] args;
	
	protected double[] cepstrum;
	protected double[] unwindowedSignal;
	
	protected ArrayList<Integer> peakBins = new ArrayList<Integer>();
	protected ArrayList<Double> peakFreqs = new ArrayList<Double>();
	protected ArrayList<Integer> aoi = new ArrayList<Integer>();
	
	protected double fundamental;
	
	protected double[] envelope;
	
	public static double cepstrumTiming = 0;
	public static double peaksTiming = 0;
	public static double interpolateTiming = 0;
	public static double FFTTiming = 0;
	public static double trigTiming = 0;
	
	public boolean isVoiceFile = false;
	
	public SignalSpectrum(double[] signal) {
		this.signal = signal;
		this.length = signal.length;
		
		double start = System.currentTimeMillis();

		// prepare for in-place FFT
		real = signal.clone();
		imag = new double[length]; // all 0s
		FFT.fft(real, imag);
		
		FFTTiming += System.currentTimeMillis() - start;
		
		// transform to polar coordinates as well
		mods = new double[length];
		args = new double[length];
		for (int i = 0; i <= length / 2; i++) {
			double trigStart = System.currentTimeMillis();
			mods[i] = Math.sqrt(real[i]*real[i] + imag[i]*imag[i]);
			args[i] = Math.atan2(imag[i], real[i]);
			trigTiming += System.currentTimeMillis() - trigStart;
		}
	

		start = System.currentTimeMillis();
		generateCepstrum();
		cepstrumTiming += System.currentTimeMillis() - start;
	}
	
	public SignalSpectrum(double[] signal, boolean isVoiceFile){
		this(signal);
		this.isVoiceFile = isVoiceFile;
	}
	
	public void setUnwindowedSignal(double[] sig){
		this.unwindowedSignal = sig;
	}
	
	private void generateCepstrum(){
		this.cepstrum = new double[length];
		for (int i = 0; i <= length/2; i++)
			cepstrum[i] = Math.log(mods[i]);
		for (int i = 1; i < length; i++)
			cepstrum[length-i] = cepstrum[i];
		FFT.fft(cepstrum, new double[length]);
	}
	
	protected void detectPeaks() {
		double start = System.currentTimeMillis();
		if(peakBins.size() > 0){
			return; //We've already done this.
		}
		
		if(!AutoTune.harmonicPeaks){
			detectPeaks_neighbors();
//			detectPeaks_stdDevs_better();
		}
		else{
			detectPeaks_harmonic();
		}
		
		peaksTiming += System.currentTimeMillis() - start;
	}
	
	// Just return the biggest bin
	private void detectPeaks_stupid() {
		double maxVal = -1;
		int maxIndex = -1;
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++) {
			if (mods[i] > maxVal) {
				maxVal = mods[i];
				maxIndex = i;
			}
		}
		peakBins.add(maxIndex);
	}
	
	private void detectPeaks_neighbors() {
		
		int numNeighborsToCheck = 3; //length/500;
		
		boolean isPeak;
		for (int i = PhaseVocoder.VOI_LOWER_BIN + numNeighborsToCheck; i <= PhaseVocoder.VUV_XOVER_BIN - numNeighborsToCheck; i++) {
			isPeak = true;
			for (int j = 1; j <= numNeighborsToCheck; j++) {
				if (mods[i] < mods[i+j] || mods[i] < mods[i-j]) {
					 isPeak = false;
				}
			}
			if (isPeak) {
				peakBins.add(i);
			}
		}
	}
	
	private void detectPeaks_stdDevs(){
		
		// need to tune these
		int STD_DEV_THRESHHOLD = 3; // TODO: peaks with k highest std devs
 		int DIFFERENCE_THRESHHOLD = 10; // 10 seems like the smallest that works well
		
		// calculate mean
		double sum = 0;
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			sum += mods[i];
		}
		double mean = sum / mods.length;
		
		// calculate stdDev
		sum = 0;
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			sum += (mods[i] - mean) * (mods[i] - mean);
		}
		double stdDev = Math.sqrt(sum / mods.length);
		
		// calculate how many stdDevs from the mean each bin is
		double[] stdDevList = new double[mods.length];
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			stdDevList[i] = (mods[i] - mean) / stdDev;
		}
		
		ArrayList<Integer> allMaxBins = new ArrayList<Integer>();
		
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			// only care about maxes, not mins
			if (stdDevList[i] > STD_DEV_THRESHHOLD){
				allMaxBins.add(i);
			}
		}		
		
		// linear clustering?
		for (int i = 0; i < allMaxBins.size() - 1; i++){
			int diff = allMaxBins.get(i + 1) - allMaxBins.get(i);
			if (diff < DIFFERENCE_THRESHHOLD){
				allMaxBins.set(i+1, (allMaxBins.get(i) + diff) / 2);
			} else{
				peakBins.add(allMaxBins.get(i));
			}
		}
	}
	
	private void detectPeaks_stdDevs_better(){
		
		int k = 5;
		
		// calculate mean
		double sum = 0;
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			sum += mods[i];
		}
		double mean = sum / mods.length;
		
		// calculate stdDev
		sum = 0;
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			sum += (mods[i] - mean) * (mods[i] - mean);
		}
		double stdDev = Math.sqrt(sum / mods.length);
		
		// calculate how many stdDevs from the mean each bin is
		double[] stdDevList = new double[mods.length];
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			stdDevList[i] = (mods[i] - mean) / stdDev;
		}
		
		int[] kMaxBins = new int[k];
		
		for (int i = 0; i < kMaxBins.length; i++){
			kMaxBins[i] = 0;
		}
		
		for (int i = PhaseVocoder.VOI_LOWER_BIN; i < PhaseVocoder.VUV_XOVER_BIN; i++){
			
			// find the index with the biggest difference, replace it if it's > 0
			double biggestDiff = 0;
			int biggestIndex = 0;
			double diff = 0;
			for (int j = 0; j < kMaxBins.length; j++){
				diff = stdDevList[i] - stdDevList[kMaxBins[j]];
				if (diff > biggestDiff){
					biggestDiff = diff;
					biggestIndex = j;
				}
			}
			
			if (biggestDiff > 0){
				kMaxBins[biggestIndex] = i;
			}
		}

		for (int i : kMaxBins){
			peakBins.add(i);
		}
	}
	
	// just add all overtones of the fundamental
	private void detectPeaks_harmonic() {
		if(fundamental <= 0){
			detectPeaks_neighbors();
			return;
		}
		double curPitch = fundamental;
		do {
			peakBins.add((int) Math.round(curPitch / AutoTune.SAMPLE_RATE * AutoTune.WINDOW_SIZE));
			curPitch += fundamental;
		} while (curPitch < PhaseVocoder.VUV_XOVER_HZ);
	}

	private double interpolate(int peakIndex) {
//		return interpolate_parabolic(peakIndex);
//		return interpolate_Quinn(peakIndex);
//		return interpolate_Jacobsen(peakIndex);
//		return interpolate_Jacobsen_Bias(peakIndex);
		return interpolate_stupid(peakIndex);
	}
	
	private double interpolate_stupid(int peakIndex) {
		return (double) peakIndex / AutoTune.WINDOW_SIZE * AutoTune.SAMPLE_RATE;
	}
	
	private double interpolate_parabolic(int peakIndex){
//		double x1 = peakIndex;
//		double y1 = real[peakIndex];
//		double x2 = peakIndex-1;
//		double y2 = real[peakIndex - 1];
//		double x3 = peakIndex+1;
//		double y3 = real[peakIndex + 1];		
//		double denom = (x1 - x2)*(x1 - x3)*(x2 - x3);
//		double A = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom;
//		double B = (x3*x3 * (y1 - y2) + x2*x2 * (y3 - y1) + x1*x1 * (y2 - y3)) / denom;
//		return -1 * B / (2*A);
//		System.out.printf("%f %f %f %f %f %f ||| %f %f %f %f \n",x2,y2,x1,y1,x3,y3,denom,A,B, -1 * B / (2*A));
		
		// This another way to do Parabolic interpolation
		double x1 = peakIndex;
		double y1 = Math.abs(real[peakIndex]);
		double x2 = peakIndex -1;
		double y2 = Math.abs(real[peakIndex - 1]);
		double x3 = peakIndex+1;
		double y3 = Math.abs(real[peakIndex + 1]);		
		
		double d = (y3-y2)/(4*y1-2*y2-2*y3);
		
//		System.out.printf(" %f %f %f %f %f %f ||| %f \n",x1,y1,x2,y2,x3,y3,d);
		return x1 +d;
		
	}
	private double interpolate_Quinn(int peakIndex){
//		double x1 = peakIndex -1;
		double y1r = real[peakIndex - 1];
		double y1i = imag[peakIndex - 1];
		double x2 = peakIndex;
		double y2r = real[peakIndex];
		double y2i = imag[peakIndex];
//		double x3 = peakIndex+1;
		double y3r = real[peakIndex + 1];	
		double y3i = imag[peakIndex + 1];	
		
		double a1 = (y1r*y2r+y1i*y2i)/(y2r*y2r+y2i*y2i);
		double a2 = (y3r*y2r+y3i*y2i)/(y2r*y2r+y2i*y2i);
		a1 = a1/(1.0-a1);
		a2 = a2/(1.0-a2);
		if (a1 >0 && a2 >0){
			return x2+a2;
		}else{
			return x2+a1;
		}
	}
	private double interpolate_Jacobsen(int peakIndex){
//		double x1 = peakIndex -1;
		double y1r = real[peakIndex - 1];
		double y1i = imag[peakIndex - 1];
		double x2 = peakIndex;
		double y2r = real[peakIndex];
		double y2i = imag[peakIndex];
//		double x3 = peakIndex+1;
		double y3r = real[peakIndex + 1];	
		double y3i = imag[peakIndex + 1];	
		
		double a1r = y1r-y3r; 
		double a1i = y1i-y3i;
		double a2r = 2*y2r - y1r -y3r;
		double a2i = 2*y2i - y1i -y3i;
		
		return x2 + (a1r*a2r+a1i*a2i)/(a2r*a2r+a2i*a2i);
	}
	private double interpolate_Jacobsen_Bias(int peakIndex){
		double x2 = peakIndex;
		return x2 + Math.tan(Math.PI/length)/(Math.PI/length)*(interpolate_Jacobsen(peakIndex)-x2);
	}
	
	protected void interpolatePeaks() {
		double start = System.currentTimeMillis();
		
		for (int peakBin : peakBins) {
			peakFreqs.add(interpolate(peakBin));
		}
		
		interpolateTiming += System.currentTimeMillis() - start;
	}
	
	
	// Finds areas of influence 
	protected void findAreasOfInfluence() {
		if(aoi.size() > 0){
			return; //we already found the aoi's
		}
		aoi.add(PhaseVocoder.VOI_LOWER_BIN);
      
		for(int i = 1; i < peakBins.size(); i++) {
			int lastPeak = peakBins.get(i-1);
			int peak = peakBins.get(i);
			int edgeOfAoi = lastPeak;
			double lowestVal = mods[lastPeak];
			for (int j = lastPeak; j < peak; j++) {
				if (mods[j] < lowestVal){
					lowestVal = mods[j];
					edgeOfAoi = j;
				}
			}
			aoi.add(edgeOfAoi);
		}
      
		aoi.add(PhaseVocoder.UNV_UPPER_BIN);
	}
	
	
	protected void estimateEnvelope() {
//		envelope_peaks();
		envelope_cepstrum();
	}
	
	protected void envelope_peaks() {
		envelope = new double[signal.length/2];
		for (int peakIndex = 0; peakIndex < peakBins.size(); peakIndex++) {
			int peakBin = peakBins.get(peakIndex);
			int lowerBoundary = (peakIndex == 0) ? 0 : aoi.get(peakIndex);
			int upperBoundary = (peakIndex == peakBins.size()-1) ? aoi.get(peakIndex+1) : length/2;
			for (int binIndex = lowerBoundary; binIndex < upperBoundary; binIndex++) {
				envelope[binIndex] = mods[peakBin];
			}
		}
	}
	
	protected void envelope_cepstrum() {
		int cutoffQuef = (int) Math.ceil(AutoTune.WINDOW_SIZE*0.10);
		double[] liftered = new double[length];
		liftered[0] = cepstrum[0];
		for (int i = 1; i <= cutoffQuef; i++) {
			liftered[i] = cepstrum[i];
			liftered[length-i] = cepstrum[length-i];
		}
		FFT.fft(liftered, new double[length]);
		envelope = liftered;
	}
}
