package autotune;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 * The Pitch detection class contains various methods used to find the fundamental frequency of a given signal
 */
class PitchDetector{

	public double totalTimeSpent;
	public double currentTime;
	//For use in Voiced/Unvoiced classification
	public MedianHeap energyList;
	public MedianHeap zeroCrossList;
	public MedianHeap peakAmpList;
	
	public boolean isVoiceFile;
	
	public static final int TOP_VOCAL_RANGE = 600;
	public static final int FLOOR_VOCAL_RANGE = 40;
	
	//For use in Voiced/Unvoiced classification: Upper and/or Lower bounds/ratios of energy, 
	//zeroCrossing and peakAmplitude, based on what I (Jerry) saw after running some files.
	double zcUp = 750;
	double eLow =0.000001;
	double paUp = 900000;
	
	double zcRatio = 10;
	double eRatio = 10;
	double paRatio = 10;

	
	//Keeps track of the pitch for the current frame and the previous frame
	private double currPitch;
	public double prevPitch;
	
	public Algorithm algorithm = Algorithm.NONE;
	
	//maximum number of candidate pitches each algorithm can generate
	public static final int maxNumberOfCandidates = 10;
	
	//public static final double FREQ_WT = .02;
	public SignalSpectrum currentSignalSpectrum;
	private double AVERAGE_WT = .5;
	
	//Weights for each algorithm: how confident we are in their ability
	private static final double FULL_AUTO_WT = 1;
	private static final double FFT_AUTO_WT = .25; //I don't think we need to use this, it's faster than Full, but it doesn't work as well. We don't care about speed that much anymore.
	private static final double HPS_WT = .25; //same reasoning as above, HPS_CEP works much better
	private static final double HPS_CEP_WT = 1.25; //Seems to work pretty well on voice files
	private static final double CEPSTRUM_WT = 0; //I don't really trust this at all, tends to do its own thing.
	private static final double AMDF_WT = .5;
	
	//How much we care what the previous pitch was
	private static final double PREV_PITCH_WT = .3;
	private static final double HALF_STEP = 0.05946; //2^(1/12) *currPitch will give you a half step
	
	//How much to weight the score of a pitch that agrees with the pitch we are looking at so that it isn't a reciprocal process.
	private static final double NEIGHBOR_WT = .75;
	
	//Average pitch that has been detected so far
	private static double averagePitch = 0;
	//number of times detectPitch has been called, used in average pitch calculation
	private static double numPitchesDetected = 0;
	
	
	/**
	 * A heap which allows O(1) lookup for the median of the heap and O(log(n)) insertion
	 */
	private class MedianHeap{
		private PriorityQueue<Double> min;
		private PriorityQueue<Double> max;
		private double median;
		
		public MedianHeap(){
			max = new PriorityQueue<Double>(Collections.reverseOrder());
			min = new PriorityQueue<Double>();
			median = -1;
		}
		
		/**
		 * Add an item to the heap O(log(n))
		 * @param n the data to be added
		 */
		public void add(double n){
			if (median == -1){
				median = n;
			}
			else if(median > n){
				min.add(median);
				max.add(n);
				median = max.poll();
			}
			else{
				max.add(median);
				min.add(n);
				median = min.poll();
			}
		}
		/**
		 * 
		 * @return the median of the dataset
		 */
		public double getMedian(){
			return median;
		}
		
	}
	


	/**
	 * holds a list of candidates and the algorithm from which they were generated
	 * A list is more useful than a priority queue for processing, but a queue is more convenient for limiting the number of candidates
	 * So make the conversion to list here
	 */
	private class AlgorithmCandidates{
		/**
		 * The algorithm which was used to generate the candidates contained in cands
		 */
        public Algorithm alg;
        /**
         * The candidate pitches and their scores.
         */
        public ArrayList<PitchScore> cands;
        
        /**
         * @param candidate	a queue of candidate PitchScores
         * @param alg		the algorithm used to generate candidate
         */
        public AlgorithmCandidates(PriorityQueue<PitchScore> candidate, Algorithm alg){
        	cands = new ArrayList<PitchScore>();
        	while(!candidate.isEmpty()){
        		cands.add(candidate.poll());
        	}
        	this.alg = alg;
        }
	}
	
	/**
	 * A class to keep track of a pitch and an associated score
	 * score will often be the peak value for the pitch
	 */
	private class PitchScore implements Comparable<PitchScore>{
            public double pitch;
            public double score;
            
            public PitchScore(double p, double s){
                pitch = p;
                score = s;
            }
            /**
             * For use in priority queue
             * @see java.lang.Comparable#compareTo(java.lang.Object)
             */
            public int compareTo(PitchScore o){
                return Double.compare(this.score, o.score);
            }
            
            public String toString(){
                return "Pitch: " + pitch + "\t Score: " + score;
            }
        }
	
	public PitchDetector(){
		energyList = new MedianHeap();
		zeroCrossList = new MedianHeap();
		peakAmpList = new MedianHeap();
		prevPitch = 0;
	}
	
	/**
	 * Detect the pitch of a given signal.
	 * 
	 * @param signal a windowed signal, given in an array of signed ints
	 * @return the fundamental pitch of signal
	 */
	public double pitchDetect(SignalSpectrum signal){
		double start = System.currentTimeMillis();
		currentTime += AutoTune.WINDOW_SIZE / AutoTune.SAMPLE_RATE / 2;
		if(isVoiceFile && numPitchesDetected > 10 && isUnvoiced(signal)){
			//Current segment is unvoiced, so we assume the pitch is the same as the previous segment
			prevPitch = currPitch;
		}
		else if(isVoiceFile && algorithm == Algorithm.NONE){ //voiced signal
			ArrayList<AlgorithmCandidates> candidatesList = new ArrayList<AlgorithmCandidates>();

			//Start Desired algorithms in their own threads:
			AlgorithmThread t2 = new AlgorithmThread(signal, Algorithm.FULL_AUTOCORRELATE);
			AlgorithmThread t3 = null;
			
			if(isVoiceFile && algorithm == Algorithm.NONE){
				//HPS + CEP can only be used on files with harmonics, it's safer not to use it on the target file.
				t3 = new AlgorithmThread(signal, Algorithm.HPS_CEP); 
			}

			//Start the threads:
			
			if(isVoiceFile){
				t3.start();
			}
			//The last thread you want to run should be called with the run() instead of start method
			t2.run();

			try {
				//wait for threads to finish
				if(isVoiceFile){
					t3.join();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				return currPitch;
			}
			//Gather results
			candidatesList.add(new AlgorithmCandidates(t2.scores, Algorithm.FULL_AUTOCORRELATE));
			if(isVoiceFile){
				candidatesList.add(new AlgorithmCandidates(t3.scores, Algorithm.HPS_CEP));
			}
			//find best pitch (pitch most standard deviations away from mean generated by its algorithm
			double p = this.findBestCandidatePitch(candidatesList, prevPitch);
			if(isVoiceFile && (p < TOP_VOCAL_RANGE && p > FLOOR_VOCAL_RANGE)){
				prevPitch = currPitch;
				this.currPitch = p;
			}
			else if(!isVoiceFile){
				//Outside of the vocal range, but not a voice file, so who cares.
				prevPitch = currPitch;
				this.currPitch = p;
			}
			else{
				//Unvoiced
				prevPitch = currPitch;
			}
			numPitchesDetected ++;
			averagePitch = averagePitch * (numPitchesDetected - 1) / numPitchesDetected + currPitch / numPitchesDetected;
		}
		else{ //A specific pitch detection algorithm was specified on the command line.
			PriorityQueue<PitchScore> scores = new PriorityQueue<PitchScore>();
			switch(algorithm){
			case FULL_AUTOCORRELATE: 
				scores = fullAutoCorrelation(signal);
				break;
			case FFT_AUTOCORRELATE:
				scores = autoCorrelation(signal);
				break;
			case HPS:
				scores = HPS(signal);
				break;
			case AMDF:
				scores = AMDF(signal);
				break;
			case CEPSTRUM:
				scores = cepstrum(signal);
				break;
			case BIGGEST_PEAK:
				scores.add(biggestPeak(signal));
				break;
			case FIRST_PEAK:
				scores.add(firstPeak(signal));
				break;
			case HPS_CEP:
				scores.add(firstPeak(signal));
				break;
			default:
				scores = fullAutoCorrelation(signal);
				break;
			}
			while(!scores.isEmpty()){
				currPitch = scores.poll().pitch;
			}
		}
		totalTimeSpent += System.currentTimeMillis() - start;
		return currPitch;
	}

	
	/**
	 * Determine the optimal pitch for this frame given the previous' frame's pitch.
	 * To be used when processing in real time.
	 * 
	 * @param candidatesList	a list of candidate pitches and their associated scores 
	 * @param prev				Previous Frame's pitch
	 * @return double the best pitch from the given candidates
	 */
    private double findBestCandidatePitch(ArrayList<AlgorithmCandidates> candidatesList, double prev) {     
        ArrayList<PitchScore> pitches = new ArrayList<PitchScore>();
        double currWt = 0;
        Algorithm currAlg;
        double score = 0;
        //calculate standard deviation for peak sizes of each pitch
        for(AlgorithmCandidates cand : candidatesList){
        	if(isVoiceFile){
        		for(int i = 0; i < cand.cands.size(); i++){
        			if(cand.cands.get(i).pitch > TOP_VOCAL_RANGE || cand.cands.get(i).pitch < FLOOR_VOCAL_RANGE){
        				cand.cands.remove(i);
        				i--;
        			}
        		}
        	}
        	currAlg = cand.alg;
        	//to avoid cumbersome code, do this in another method
        	currWt = getWeight(currAlg);
        	
        	double mean = 0;
        	for(int i = 0; i < cand.cands.size(); i++){
        		mean += cand.cands.get(i).score;
        	}
        	mean /= cand.cands.size(); //mean
        	
        	int sum = 0;
        	for(int i = 0; i < cand.cands.size(); i++){
        		sum += (cand.cands.size() - mean) * (cand.cands.size() - mean);
        	}
        	if(cand.cands.size() == 0){
        		sum = 0;
        	}
        	else{
            	sum /= cand.cands.size(); //standard deviation
        	}
        	for(int i = 0; i < cand.cands.size(); i++){
        		if(sum == 0){
        			continue;
        		}
        		score = ((cand.cands.get(i).score - mean) / sum) * currWt; //# of standard deviations from the mean (of the candidate pitch scores) times the weight of the algorithm
        		if(score < 0){
        			continue;
        		}
        		//Try and find artificial pitch doubling or halving
        		//If the prev is an octave lower +- a half step
        		if(cand.cands.get(i).pitch / prev > 2 - HALF_STEP && cand.cands.get(i).pitch / prev < 2 + HALF_STEP){
        			score -= PREV_PITCH_WT * score;
        		}
        		//if the prev is twice as much +- a half step (artificial halving)
        		if(prev / cand.cands.get(i).pitch > 2 - HALF_STEP && prev / cand.cands.get(i).pitch < 2 + HALF_STEP){
        			score -= PREV_PITCH_WT * score;
        		}
        		if(prev/cand.cands.get(i).pitch > 2 || prev/cand.cands.get(i).pitch < .5){
        			score -= PREV_PITCH_WT * 2 * score;
        		}
        		if(numPitchesDetected > 10 && cand.cands.get(i).pitch / averagePitch > 2){
        			score -= AVERAGE_WT * score;
        		}
        		else if(numPitchesDetected > 10 && cand.cands.get(i).pitch / averagePitch < .5){
        			score -= AVERAGE_WT * score;
        		}

        		//The list will contain a series of pitches and associated scores, which are the standard deviations adjusted for algorithm preference
        		pitches.add(new PitchScore(cand.cands.get(i).pitch, score)); //number of stand deviations from the mean
        	}
        }
        
        //There is probably a faster way to do this using previous loops, but the size of pitches is quite small
        //For every pitch: find if there is another candidate pitch from another algorithm, if there is, combine the scores
        for(int i = 0; i < pitches.size(); i++){
        	for(int j = i; j < pitches.size(); j++){
        		//if they are within quarter steps
        		if(pitches.get(i).pitch / pitches.get(j).pitch > 1 - (HALF_STEP / 8) && pitches.get(i).pitch / pitches.get(j).pitch < 1 + (HALF_STEP / 8)){
        			pitches.get(i).score = pitches.get(i).score + (NEIGHBOR_WT * pitches.get(j).score);
        			pitches.get(j).score = pitches.get(j).score + (NEIGHBOR_WT * pitches.get(i).score);
        		}
        	}
        }
        double maxPitch = 0;
        double maxScore = 0;
        for(PitchScore p : pitches){
        	if(p.score > maxScore){
        		maxPitch = p.pitch;
        		maxScore = p.score;
        	}
        }
        return maxPitch;
   }

    /**
     * Finds the weight for an algorithm
     * 
     * @param a		the algorithm in use
     * @return 		the weight for a given algorithm
     */
    private double getWeight(Algorithm a){
    	switch(a){
    		case FULL_AUTOCORRELATE:
    			return FULL_AUTO_WT;
    		case FFT_AUTOCORRELATE:
    			return FFT_AUTO_WT;
    		case HPS:
    			return HPS_WT;
    		case AMDF:
    			return AMDF_WT;
    		case CEPSTRUM:
    			return CEPSTRUM_WT;
    		case HPS_CEP:
    			return HPS_CEP_WT;
    		default:
    			return 0;
    	}
    }
    
	/**
	 * Determine if the current signal frame is unvoiced based on Cepstral Peak size, Zero crossing rate, and the Energy of the signal.
	 * 
	 * @param signal The current frame of the signal
	 * @return boolean of whether we think the current signal frame is voiced.
	 */
    private boolean isUnvoiced(SignalSpectrum signal){
		double[] sig = signal.signal;
		
		double zeroCrosses = zeroCrosses(sig);
		zeroCrossList.add(zeroCrosses);
		
		double energy = energy(sig);
		energyList.add(energy);
		
		double cepAmp = cepstrumPeakAmplitude(signal);
		peakAmpList.add(cepstrumPeakAmplitude(signal));
		
		double zcMed =  zeroCrossList.getMedian();
		double eMed =  energyList.getMedian();
		double paMed =  peakAmpList.getMedian();
		
		boolean isUnvoiced = false;
//		System.out.println(zeroCrossList.getMedian()+" "+energyList.getMedian()+" "+peakAmpList.getMedian());
		
		double margin = .95;
		if ((cepAmp < paMed/margin) && 
				(zeroCrosses > zcMed*margin) && 
				(energy < eMed/margin)
				) isUnvoiced = true;
		
		/*if (zeroCrosses > zcUp || zeroCrosses > zcMed*zcRatio) isUnvoiced = true;
		if (energy < eLow || energy < eMed/eRatio) isUnvoiced = true;
		if (cepAmp > paUp|| cepAmp < paMed/paRatio) isUnvoiced = true;
		*/
		
		return isUnvoiced;
	}
	
    /**
     * Find the energy of the current frame (Defined as the sum of each sample in the signal squared)
     * 
     * @param signal the current frame
     * @return the energy of the current frame
     */
	private double energy(double[] signal){
		double sum = 0;
		for(int i = 0; i < signal.length; i++){
			sum += signal[i] * signal[i];
		}
		return sum;
	}
	
	/**
	 * Find the number of times the signal crosses 0 in the frame.
	 * 
	 * @param signal The current frame
	 * @return the number of zero crosses
	 */
	private double zeroCrosses(double[] signal){
		int numCrosses = 0;
		double curr = signal[1];
		double prev = signal[0];
		int firstCross = -1;
		//System.out.println(signal.length);
		for(int i = 1; i < signal.length; i++){
			curr = signal[i];
			prev = signal[i - 1];
			//If curr and prev have different signs, then it's a zero crossing
			if(curr <= 0 && prev > 0){
				numCrosses += 1;
				if(firstCross == -1){
					firstCross = i;
				}
			}
			else if(prev <= 0 && curr > 0){
				numCrosses += 1;
				if(firstCross == -1){
					firstCross = i;
				}
			}
		}
		return numCrosses;
	}
	/**
	 * A very simple algorithm to detect fundamental pitch. Inherently flawed in various respects. Do not use.
	 * 
	 * @param signal a windowed signal, given in an array of signed ints
	 * @return the fundamental pitch of signal found using a zero crossing method
	 */
	@SuppressWarnings("unused")
	private double zeroCrossPitch(double[] signal){
		double numCrosses = zeroCrosses(signal);
		double crossesPerTimeStep = (double)(numCrosses) / signal.length;
		//System.out.println(crossesPerTimeStep * AutoTune.SAMPLE_RATE / 2);
		return crossesPerTimeStep * AutoTune.SAMPLE_RATE / 2;
	}
	
	/**
	 * AutoCorrelation should accurately detect pitches generally on all types of waves. 
	 * Uses an AutoCorrelation method to determine pitch of a signal.
	 * Calculated quickly using FFT's and complex conjugates
	 * @param signal The current SignalSpectrum
	 * @return PriorityQueue containing candidates for the fundamental frequency
	 */
    private PriorityQueue<PitchScore> autoCorrelation(SignalSpectrum signal) {
        PriorityQueue<PitchScore> scores = new PriorityQueue<PitchScore>();
        
        double[] real = signal.mods.clone();
        double[] imag = new double[signal.length]; // all 0s
        FFT.ifft(real, imag);
        scores = detectPeaksDeriv(real);
        for(PitchScore s : scores){
        	s.pitch = AutoTune.SAMPLE_RATE / s.pitch;
        }
        return scores;
    }
	
    private double[] HPS_Values(SignalSpectrum sig,int compress){
		int R = compress;
		double[] spectrum = sig.mods.clone();
		double[][] compressedSpectrums =   new double[R][];
		compressedSpectrums[0] = spectrum;
		for (int i = 1; i < R;i++){
			double[] compressedSpec = new double[spectrum.length/(i+1)];
			for (int j =0;j <spectrum.length/(i+1); j++){
				compressedSpec[j] = spectrum[(i+1)*j];
			}
			compressedSpectrums[i] =compressedSpec;
		}
		
		double[] multipledSpectrum = new double[compressedSpectrums[R-1].length];
		double prod;
		for (int i =0; i < compressedSpectrums[R-1].length;i++ ){
			prod = 1;
			for (int j =0;j<R;j++){
				prod *= compressedSpectrums[j][i];
			}
			multipledSpectrum[i] = prod;
		}
//		for (int i =0; i < multipledSpectrum.length;i++ ){
//			System.out.println(multipledSpectrum[i]);
//		}
		return multipledSpectrum;
	}
	private  PriorityQueue<PitchScore> HPS(SignalSpectrum sig){
//		double[] multipledSpectrum = HPS_Values(sig,3);
//		
//		int numNeighborsToCheck = 3;
//		boolean isPeak;
//		
//		PriorityQueue<PitchScore> scores = new PriorityQueue<PitchScore>();
//		for (int i = numNeighborsToCheck; i <= multipledSpectrum.length/2 - numNeighborsToCheck; i++) {
//			isPeak = true;
//			for (int j = 1; j < numNeighborsToCheck; j++) {
//				if (multipledSpectrum[i] < multipledSpectrum[i+j] || multipledSpectrum[i] < multipledSpectrum[i-j]) {
//					 isPeak = false;
//				}
//			}
//			
//			if (isPeak) {
////				System.out.println(i+"  "+ (double)AutoTune.SAMPLE_RATE / AutoTune.WINDOW_SIZE*i +"  "+multipledSpectrum[i]);
//				scores.add(new PitchScore((double) AutoTune.SAMPLE_RATE / AutoTune.WINDOW_SIZE *i, multipledSpectrum[i]));
//				if(scores.size() > 1){
//	                scores.poll();
//	            }
//			}
//		}
//		for (PitchScore pitchScore :scores){
//			System.out.println(pitchScore.pitch +" " +pitchScore.score );
//		}
		PriorityQueue<PitchScore> scores = Cep_HPS(sig);
//		System.exit(1);
		return scores;
	}
	/**
	 * Cepstrum-Biased HPS. Combining Cesptrum and HPS by 
	 * See https://ccrma.stanford.edu/~pdelac/research/MyPublishedPapers/icmc_2001-pitch_best.pdf for details
	 *
	 * @param sig the signal
	 * @return A set of candidate pitches and their scores in a priority queue.
	 */
	private PriorityQueue<PitchScore> Cep_HPS(SignalSpectrum sig){
		/// fic is Frequency Indexed Cepstrum 
		double [] cep = sig.cepstrum.clone();
		double [] FIC = new double[cep.length];
		int N = FIC.length;
		double [] HPS = HPS_Values(sig,3);
		int R  = HPS.length;
		 	
		int index;
		for (int i =1; i < N; i++){
			 index = (int) N / i;
			 if (index < R && cep[i]>FIC[index]){
				 FIC[index] +=cep[i];
			 }
		 }
		 for (int i =1; i < R-1; i++){
//			 System.out.println(cep[i] );
		 }
	
		 for (int i =1; i < R; i++){
			 HPS[i] = HPS[i]*FIC[i];
		 } 
		 
		 PriorityQueue<PitchScore> scores = detectPeaksDeriv(HPS);
		 for(PitchScore s : scores){
				s.pitch = AutoTune.SAMPLE_RATE / AutoTune.WINDOW_SIZE * s.pitch;
			}
		 return scores;
	}
	/**
	 *Calculates the Average Magnitude Difference Function, similar to autocorrelation but 
	 *finding the difference between a delayed signal with current signal
	 * @param signal the current signal frame
	 * @return PriorityQueue containing candidates for the fundamental frequency
	 */
	private PriorityQueue<PitchScore> AMDF(SignalSpectrum signal) {
    	double[] samples = signal.unwindowedSignal.clone();
		double[] D = new double[signal.length];
		int N =  D.length;
		double sum;
		for(int k = 0; k < N-1; k++){
			sum = 0;
			for(int n = 1; n < N-k-1; n++){
				sum += Math.abs(samples[n] - samples[n+k]);
			}
			D[k] = 1.0/(N-k-1)*sum;
		}
		PriorityQueue<PitchScore> scores = new PriorityQueue<PitchScore>(maxNumberOfCandidates);
		int numNeighbors = 3;
		boolean isValley;
		for (int i = numNeighbors; i <= D.length - numNeighbors; i++) {
			//System.out.println( i +" "+D[i]);
			isValley = true;
			for (int j = 1; j < numNeighbors; j++) {
				if (D[i] > D[i+j] || D[i] > D[i-j]) {
					isValley = false;
				}
			}
			if (isValley) {
				PitchScore pScore = new PitchScore((double) AutoTune.SAMPLE_RATE / i, 1.0/D[i]);
				scores.offer(pScore);
			}
		}
		return scores;
    }
	
	/**
	 * Iteratively calculates the autocorrelation of a signal. Nothing fancy is done. O(n^2). Rather slow, but high resolution.
	 * 
	 * @param sig current signal frame
	 * @return A set of candidate pitches and their scores in a priority queue.
	 */
	private PriorityQueue<PitchScore> fullAutoCorrelation(SignalSpectrum sig){
		PriorityQueue<PitchScore> scores;
		
		double[] signal = sig.unwindowedSignal.clone();
		double[] R = new double[signal.length];
		double sum;
		for(int i = 0; i < R.length; i++){
			sum = 0;
			for(int j = 1; j < signal.length - i; j++){
				sum += signal[j] * signal[j+i];
			}
			R[i] = sum;
		}
		scores = detectPeaksDeriv(R);
		for(PitchScore s : scores){
			s.pitch = AutoTune.SAMPLE_RATE / s.pitch;
		}
		
		return scores;
	}
	
	/**
	 * Calculates pitch candidates based on the cepstrum of a signal. Defined as the IFFT of the log of the magnitude spectrum of a signal
	 * @see SignalSpectrum#cepstrum
	 * @param signal current signal frame
	 * @return A set of candidate pitches and their scores in a priority queue.
	 */
	private PriorityQueue<PitchScore> cepstrum(SignalSpectrum signal) {	
		PriorityQueue<PitchScore> scores = detectPeaksDeriv(signal.cepstrum);
		for(PitchScore s : scores){
			s.pitch = AutoTune.SAMPLE_RATE / s.pitch;
		}
		
		return scores;
	}
	
	
	/**
	 * Detects Peaks in an array using the derivative
	 * @param arr the array generated from a pitch detecting algorithm
	 * @return PriorityQueue a queue that contains THE INDEX  of the peak and the peak value.
	 * Note that the pitch detection algorithm is required to convert the index to pitch value.
	 */
	private PriorityQueue<PitchScore> detectPeaksDeriv(double[] arr) {
		double diff, diffPrev = -1;
		PriorityQueue<PitchScore> scores = new PriorityQueue<PitchScore>();
		
		for (int i = 1; i < arr.length; i++) {
			diff = arr[i] - arr[i - 1];
			if (diff < 0 && diffPrev > 0){
				scores.add(new PitchScore(i, arr[i]));
				if(scores.size() > maxNumberOfCandidates){
					scores.poll();
				}
			}
			diffPrev = diff;
		}
		return scores;
	}
	
	/**
	 * Calculate the highest cepstral peak for used in voice/unvoiced classification.
	 * 
	 * @param signal the signal
	 * @return the value of the highest cepstral peak
	 * @see PitchDetector#isUnvoiced(SignalSpectrum)
	 * @see SignalSpectrum#cepstrum
	 */
	private double cepstrumPeakAmplitude(SignalSpectrum signal){
		double[] cep = signal.cepstrum.clone();
		
		double max = -1000000;
		for (int i = 1; i < cep.length/2; i++) {
			if (cep[i] > max && cep[i] > cep[i-1]){
				 max = cep[i];
			}
		}
		return max;
	}
	/**
	 * Find pitch based off of the biggest peak in the signal spectrum. Do not use.
	 * 
	 * @param signal the signal
	 * @return the pitchscore of the biggest peak
	 */
	private PitchScore biggestPeak(SignalSpectrum signal){
		double maxVal = 0;
		int maxIndex = 0;
		for(int i = 1; i < signal.real.length / 2; i++){
			if(signal.real[i] > maxVal && signal.real[i] > signal.real[i-1] && signal.real[i] > signal.real[i+1]){
				maxVal = signal.real[i];
				maxIndex = i;
			}
		}
		return new PitchScore(AutoTune.SAMPLE_RATE / AutoTune.WINDOW_SIZE * maxIndex, 1);
	}
	/**
	 * Find pitch based off of the first peak in the signal spectrum. Do not use.
	 * 
	 * @param signal don't use this
	 * @return don't use this
	 */
	private PitchScore firstPeak(SignalSpectrum signal){
		for(int i = 1; i < signal.real.length / 2; i++){
			if(signal.real[i] > signal.real[i-1] && signal.real[i] > signal.real[i-1]){
				return new PitchScore(AutoTune.SAMPLE_RATE / AutoTune.WINDOW_SIZE * i, 1);
			}
		}
		return new PitchScore(0, 0);
	}

	/**
	 * A thread where PitchDetection algorithms are run.
	 * @see PitchDetector#detectPeaksDeriv(double[])
	 */
	private class AlgorithmThread extends Thread{
		public PriorityQueue<PitchScore> scores;
		public SignalSpectrum sig;
		public Algorithm alg;
		
		public AlgorithmThread(SignalSpectrum s, Algorithm a){
			sig = s;
			alg = a;
		}
		
		@Override
		public void run(){
			switch(alg){
				case FULL_AUTOCORRELATE:
					scores = fullAutoCorrelation(sig);
					break;
				case FFT_AUTOCORRELATE:
					scores = autoCorrelation(sig);
					break;
				case HPS:
					scores = HPS(sig);
					break;
				case AMDF:
					scores = AMDF(sig);
					break;
				case CEPSTRUM:
					scores = cepstrum(sig);
					break;
				case HPS_CEP:
					scores = Cep_HPS(sig);
					break;
				default:
					//Shouldn't get here
					scores = null;
					break;
			}
		}
	}
}
