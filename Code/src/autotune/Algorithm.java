package autotune;

/**
 * Enumerator for the pitch detection algorithms
 */
public enum Algorithm{
	/**
	 * A version of autocorrelation using an FFT to quickly calculate correlation<p>
	 * Can be used on all files
	 * @see PitchDetector#autoCorrelation(SignalSpectrum)
	 */
	FFT_AUTOCORRELATE, 
	
	/**
	 * A version of autocorrelation based on the slower calculation, which gets more precise answers with higher resolution <p>
	 * Can be used on all files
	 * @see PitchDetector#fullAutoCorrelation(SignalSpectrum)
	 */
	FULL_AUTOCORRELATE, 
	
	/**
	 * A Cepstrum analysis, which is defined as the FFT of the log magnitude spectrum of the signal <p>
	 * <b> NOTE: only to be used on waves with harmonics </b>
	 * @see PitchDetector#cepstrum(SignalSpectrum)
	 */
	CEPSTRUM, 
	
	/**
	 * Harmonic Product Spectrum <p>
	 * <b>NOTE: can only be used on waves with harmonics</b>
	 * @see PitchDetector#HPS(SignalSpectrum)
	 */
	HPS, 
	
	/**
	 * Average magnitude difference function <p>
	 * Can be used on all files
	 * @see PitchDetector#AMDF(SignalSpectrum)
	 */
	AMDF,
	
	/**
	 * Finds the biggest peak in the FFT and returns the pitch associated with it <p>
	 * <b>NOTE: Don't use this one. It doesn't work. </b>
	 * @see PitchDetector#biggestPeak(SignalSpectrum)
	 */
	BIGGEST_PEAK,
	
	/**
	 * Finds the first peak in the FFT and returns the pitch associated with it. <p>
	 * <b> NOTE: Don't use this one either. </b>
	 * @see PitchDetector#firstPeak(SignalSpectrum)
	 */
	FIRST_PEAK,
	
	/**
	 * Uses the Cepstrum to weight the HPS <p>
	 * Can only be used on waves with harmonics
	 * @see PitchDetector#Cep_HPS
	 */
	HPS_CEP,
	
	/**
	 * No Algorithm selected, default algorithms to be used.
	 */
	NONE
}