package autotune;

import org.kohsuke.args4j.*;
/**
 * A class which holds command line options and their usage.
 */
public class Options {
	
	/**
	 * The name of the source file. The source file is the file that will be autotuned.
	 */
	@Option(name = "-s", aliases = {"--src", "--source", "--srcFile"}, 
			required = false, usage = "Use given source, target, and output files",
			forbids = {"-l"})
	protected String srcFile;
	
	/**
	 * The name of the target file. The target file is the file that the source file will be autotuned to.
	 */
	@Option(name = "-t", aliases = {"--tgt", "--target", "--tgtFile"},  
			required = false, usage = "Use given source, target, and output files")
	protected String tgtFile;
	
	/**
	 * The name of the output file.
	 */
	@Option(name = "-o", aliases = {"--out", "--outFile"}, 
			required = true, usage = "Use given source, target, and output files")
	protected String outFile;
	
	/**
	 * The size of the window used in analysis. <p>
	 * Note: Must be a power of 2. 2048 is the preferred and probably default window size.
	 */
	@Option(name = "-w", aliases = {"--window", "--windowSize"},
			required = false, usage = "Use given window size")
	protected int windowSize = AutoTune.WINDOW_SIZE;
	
	/**
	 * Outputs results to speakers in realtime.
	 */
	@Option(name = "-r", aliases = {"--real", "--realtime"},
			required = false, usage = "Use realtime input from microphone + MIDI device")
	protected boolean realtime = false;
	
	/**
	 * Length in seconds for realtime input.
	 */
	@Option(name = "-l", aliases = {"--length"}, required = false,
			usage = "End realtime input after given number of seconds",
			forbids = {"-s"})
	protected int length = 60;
	
	/**
	 * Set the beta for PhaseVocoder. A beta of 2 will shift the source file up an octave, a beta of .5 will shift the source file down an octave.
	 * @see PhaseVocoder
	 */
	@Option(name = "-b", aliases = ("--beta"), 
			required = false, usage = "Use given beta",
					forbids = {"-t"})
	protected double beta = -1.0;
	
	/**
	 * Includes the original source file in the output file, creating a harmonizing effect. Can be used with the interval and beta options for good effect.
	 */
	@Option(name = "-h", aliases = ("--harmonize"),
			required = false, usage = "Play the source as well as the autotuned output on the same file",
					forbids = {"-t"})
	protected boolean harmonize = false;
	
	/**
	 * Set an interval for the pitch to be shifted. A more convenient way of setting beta than -b. <p>
	 * Options are the short form description. For instance, m2 is minor second, M2 is major second, P4 is perfect fourth.
	 */
	@Option(name = "-i", aliases = ("--interval"),
			required = false, usage = "Select an interval to shift the pitch. Options are the short form description, for instance, m2 is minor second, M2 is major second, P4 is perfect fourth.",
					forbids = {"-t"})
	protected String interval = "none";
	
	/**
	 * Set the algorithm to be used to detect pitch on the source file. Be careful to use appropriate algorithms for pure sine wave files or files without harmonics.
	 * @see PitchDetector
	 */
	@Option(name = "-y", aliases = {"--as", "--algorithmSource"},
			required = false, usage = "Choose which pitch detection algorithm is run on the source file. Options: FullAutoCorrelation, FFTAutoCorrelation, AMDF, HPS, CEPSTRUM")
	protected String algSrc;
	
	/**
	 * Set the algorithm to be used to detect pitch on the target file. Be careful to use appropriate algorithms for pure sine wave files or files without harmonics.
	 * @see PitchDetector
	 */
	@Option(name = "-z", aliases = {"--at", "--algorithmTarget"},
			required = false, usage = "Choose which pitch detection algorithm is run on the target file. Options: FullAutoCorrelation, FFTAutoCorrelation, AMDF, HPS, CEPSTRUM")
	protected String algTgt;
	
	/**
	 * Print out a timing profile upon completion.
	 */
	@Option(name = "-e", aliases = ("--time"),
		required = false, usage = "Print timing analyses for different aspects of the program.")
	protected boolean timing;
	
	@Option(name = "-m", aliases = ("--multinote"),
			required = false, usage = "For use with midi input, the source file will be autotuned to all notes pressed. Must be used with real time target.",
					forbids = {"-t"})
	protected boolean multiNote = true;
	
	@Option(name = "-p", aliases = ("--tpain"),
			required = false, usage = "Sound like T-pain. Autotune the source file to the closest half step.")
	protected boolean tpain;
	
	@Option(name = "-d", aliases = ("--debug"),
			required = false, usage = "Print debug statements")
	protected boolean debug = false;
	
	@Option(name = "-n", aliases = {"--notVoiced", "--notVoicedSource"},
			required = false, usage = "The source file is not a voice file")
	protected boolean nvs = false;
	
	@Option(name = "-g", aliases = ("--graphPitch"),
			required = false, usage = "Graph pitches")
	protected boolean graph = false;
	
	@Option(name = "-x", aliases = {"--harm", "--harmonix", "--harmonicPeakDetectionAlgorithm"},
			required = false, usage = "Use harmonic peak detection algorithm instead of neighbors")
	protected boolean harmonicPeaks = false;
}
