package autotune;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.stage.Stage;

public class Visualizer extends Application{
	
	public static ArrayList<XYChart.Series<Number,Number>> seriesList =new ArrayList<XYChart.Series<Number,Number>>();
	public  static String xAxis;
	public  static String yAxis; 
	public  static String title;
	
	public static void main(String[] args) {
        launch(args);
    }
	
	public Visualizer(String title, String xAxis, String yAxis){
		this.xAxis = xAxis;
		this.yAxis = yAxis;
		this.title = title;
//		seriesList = new ArrayList<XYChart.Series<Number,Number>>();
	}
	public Visualizer(){
	}
	
	public void addSeries(String name, double[] xValues,double[] yValues){
		assert xValues.length == yValues.length : 
			"X and Y do not have equal sets of values"; 
		
		XYChart.Series<Number,Number> series = new XYChart.Series();
		series.setName(name);
		for (int i =0; i <xValues.length;i++){
	        	series.getData().add(new Data<Number, Number>(xValues[i], yValues[i]));
		 }
		seriesList.add(series);
		
	}
	public void addSeries(String name,double[] yValues){
		
		XYChart.Series<Number,Number> series = new XYChart.Series();
		series.setName(name);

		
		for (int i =0; i <yValues.length;i++){
	        	series.getData().add(new Data<Number, Number>(i, yValues[i]));
		 }
		seriesList.add(series);
	}
	
	public void start(Stage stage) {
        stage.setTitle(title);
//      defining the axes
        final NumberAxis x_axis = new NumberAxis();
        final NumberAxis y_axis = new NumberAxis();
        x_axis.setLabel(xAxis);
        y_axis.setLabel(yAxis);
        
        //creating the chart
        final LineChart<Number,Number> lineChart = new LineChart<Number,Number>(x_axis,y_axis);    
        lineChart.setTitle(title);
        lineChart.setCreateSymbols(false);
        Scene scene  = new Scene(lineChart,800,600);
        for(XYChart.Series<Number,Number> series: seriesList){
        	lineChart.getData().add(series);
        }
   
        stage.setScene(scene);
        stage.show();
    }
	 public void print(){
		 System.out.println("sdfsdfsdfdsfsdfds");
	 }
}
