

package autotune;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

import javax.sound.sampled.*;
import javax.sound.midi.*;
/**
 * The controlling class for autotuning. <p>
 * 1. read in the command line arguments and set the appropriate constants <p>
 * 2. Open the appropriate streams and prepare for reading and writing <p>
 * 3. AutoTune loop, pitch detect, phasevocode, write file, repeat.
 * @see PitchDetector
 * @see PhaseVocoder
 */
public class AutoTune {
	/**
	 * Only allow 16-bit mono WAV audio input @ 44.1 kHz
	 */
	protected static final AudioFormat REQUIRED_AUDIO_FORMAT = new AudioFormat(44100, 16, 1, true, false);


	/**
	 * Number of samples per second. 44100hz is required.
	 */
	protected static final float SAMPLE_RATE     = REQUIRED_AUDIO_FORMAT.getSampleRate();
	private static final int   BYTES_PER_FRAME = REQUIRED_AUDIO_FORMAT.getFrameSize();
	private static final int   NUM_CHANNELS    = REQUIRED_AUDIO_FORMAT.getChannels();
	private static final int   MAX_SAMPLE_VAL  = (int) Math.pow(2, 8*BYTES_PER_FRAME - 1);

	/**
	 * Windowing constant: Size of the window where analysis occurs. <p>
	 * May be set by the command line option -w. 
	 * @see Options#windowSize
	 */
	protected static int      WINDOW_SIZE   = 2048; // in samples, must be a power of 2
	/**
	 * Window constant: The amount of each window that overlaps with the previous window. <p>
	 * <b> Must be a power of two, less than 1. </b>
	 * For example, if HOP_FRACTION = .5, half of the next window will overlap with half of the current window.
	 */
	protected static float    HOP_FRACTION  = 0.5f;
	private static double[] GCW_COEFS     = {0.5, 0.5};
	private static double[] WINDOW_FUNC;

	// Chunk constants
	private static int NUM_CHUNKS; // per window
	private static int CHUNK_SIZE; // in samples
	private static int BYTES_PER_CHUNK;

	// I/O
	private static AudioInputStream srcStream, tgtStream;
	private static MidiListener tgtMidiListener;
	private static OutputStream outStream;
	private static SourceDataLine outDataLine;

	// Byte buffers for read-in
	private static byte[] srcBuffer;
	private static byte[] tgtBuffer;
	private static byte[] outBuffer;

	// These will hold the actual windows being processed at any point in time
	private static double[] srcWindow;
	private static double[] tgtWindow;
	private static double[] outWindow;

	// Each is a list of chunks, i.e. partitioning the window at hop-intervals
	private static double[][] srcChunks;
	private static double[][] tgtChunks;
	private static double[][] outChunks;

	/**
	 * Whether or not the program will autotune real-time mic and MIDI input or two already-made files
	 * @see Options#realtime
	 */
	protected static boolean realtime;
	protected static boolean realtimeSrc;
	protected static boolean realtimeTgt;

	protected static long maxLength;
	protected static long length = 0;
	
	protected static String outFile;

	protected static boolean debug;
	
	protected static boolean harmonicPeaks;
	

	/**
	 * Whether or not to print a timing profile of methods. Can be set to true with -time
	 * @see Options#timing
	 */
	protected static boolean timing;
	private static double SignalSpectrumInitTime;

	private static boolean isRunning;

	private static double windowTime;
	private static Scanner in;
	private static TargetDataLine srcDataLine;
	/**
	 * Method for gracefully exiting. Closes all open streams and ends the program.
	 * @param status The status of the program at time of destruction
	 */
	private static void destruct(int status) {
		// Close any open streams
		try { srcStream.close(); } catch (Exception e) {}
		try { tgtStream.close(); } catch (Exception e) {}
		try { outStream.close(); } catch (Exception e) {}
		try { tgtMidiListener.close(); } catch (Exception e) {}
		if (status == 0) System.out.println("Done!");
		else{
			System.out.println("Unexpected exit. Run with -debug for further information.");
		}
		System.exit(status);
	}

	/**
	 * Converts the bytes in the given buffer to doubles on [-1, 1), placing them in the given chunk
	 * @param buffer Buffer to convert
	 * @param chunk chunk to place read bytes into
	 */
	private static void readChunkFromBuffer(byte[] buffer, double[] chunk) {
		for (int i = 0; i < CHUNK_SIZE; i++) {
			// cast to short first so it knows it's signed
			short sample  = (short) (((buffer[2*i+1] & 0xff) << 8) | (buffer[2*i] & 0xff));
			chunk[i] = (double) sample / MAX_SAMPLE_VAL;
		}
	}


	/**
	 * For offline processing:
	 * Opens two input files and an output file.
	 * For realtime processing:
	 * Opens an input line, a MIDI listener, and an output line to the speaker.
	 * If it fails, it ends the program.
	 * @param opt The set of command line options from startup
	 */
	private static void initStreams(Options opt) {
		long len = 0;
	    if (opt.srcFile != null) {
	      try{
	        srcStream = openWAV( opt.srcFile);
	        len = srcStream.getFrameLength(); // WINDOW_SIZE * ((srcStream.getFrameLength()-1) / WINDOW_SIZE + 1);
	      } catch (IOException | UnsupportedAudioFileException e) {
	        System.err.println("Error opening file " + e.getMessage());
	        if(debug){
	          e.printStackTrace();
	        }
	        destruct(1);
	      }
	    } else {
	      DataLine.Info srcInfo = new DataLine.Info(TargetDataLine.class, REQUIRED_AUDIO_FORMAT); 
	      try {
	        srcDataLine = (TargetDataLine) AudioSystem.getLine(srcInfo);
	        srcDataLine.open(REQUIRED_AUDIO_FORMAT, CHUNK_SIZE);
	        srcStream = new AudioInputStream(srcDataLine);
	        len = maxLength;
	      } catch (LineUnavailableException e) {
	        System.out.println("Error: No available microphone input.");
	        if(debug){
	          e.printStackTrace();
	        }
	        destruct(1);
	      }
	    }
	    if (opt.tgtFile != null) {
	      try {
	        tgtStream = openWAV( opt.tgtFile);
	      } catch (IOException | UnsupportedAudioFileException e) {
	        System.err.println("Error opening file " + e.getMessage());
	        if(debug){
	          e.printStackTrace();
	        }
	        destruct(1);
	      }
	    } else if (realtimeTgt){
	      try {
	        tgtMidiListener = new MidiListener();
	      } catch (MidiUnavailableException e) {
	        System.out.println("Error: No available MIDI devices.");
	        if(debug){
	          e.printStackTrace();
	        }
	        destruct(1);
	      }
	    }
	    if (opt.realtime) {
	      DataLine.Info outInfo = new DataLine.Info(SourceDataLine.class, REQUIRED_AUDIO_FORMAT); 
	      try {
	        outDataLine = (SourceDataLine) AudioSystem.getLine(outInfo);
	        outDataLine.open(REQUIRED_AUDIO_FORMAT);
	        outDataLine.start();
	      } catch (LineUnavailableException e) {
	        System.out.println("Error: No available output line.");
	        if(debug){
	          e.printStackTrace();
	        }
	        destruct(1);
	      }
	    }
	    try {
	      if (realtimeSrc || realtimeTgt) {
	        outStream = new BufferedOutputStream(new FileOutputStream("TEMP.wav"));
	      } else {
	        outStream = new BufferedOutputStream(new FileOutputStream(outFile));
	      }
	      AudioSystem.write(
	          new AudioInputStream(new ByteArrayInputStream(new byte[0]), REQUIRED_AUDIO_FORMAT, len), 
	          AudioFileFormat.Type.WAVE, 
	          outStream);
	    } catch (IOException e) {
	      System.err.println("Error opening file " + e.getMessage());
	      if(debug){
	        e.printStackTrace();
	      }
	      destruct(1);
	    }
	}

	/**
	 * Disassembles the given chunk's ints into bytes and write to outStream
	 * @param chunk the chunk to write to output
	 */
	private static void writeChunkToOut(double[] chunk) {
		for (int i = 0; i < CHUNK_SIZE; i++) {
		      int sample = (int) Math.round(chunk[i] * MAX_SAMPLE_VAL);

		      // clip if necessary
		      if (sample >= MAX_SAMPLE_VAL) sample = MAX_SAMPLE_VAL-1;
		      if (sample < -MAX_SAMPLE_VAL) sample = -MAX_SAMPLE_VAL;

		      outBuffer[2*i]   = (byte) (sample & 0xff);
		      outBuffer[2*i+1] = (byte) (sample >> 8);
		    }
		    try {
		      length += CHUNK_SIZE;
		      if (length > maxLength) {
		        if (realtimeTgt || realtimeSrc) {
		          correctFileLength();
		        }
		        destruct(0);
		      }
		      outStream.write(outBuffer, 0, BYTES_PER_CHUNK);
		      if (realtime) {
		        outDataLine.write(outBuffer, 0, BYTES_PER_CHUNK);
		      }
		    } catch (IOException e) { // should never happen...
		      if(debug){
		        e.printStackTrace();
		      }
		      destruct(1);
		    }
	}

	//	private static void doublesToBytes(double[] bytes){
	//		new double[] byteBuffer = new double[BYTES_];
	//		for (int i = 0; i < bytes.length; i++) {
	//			int sample = (int) Math.round(bytes[i] * MAX_SAMPLE_VAL);
	//			
	//			// clip if necessary
	//			if (sample >= MAX_SAMPLE_VAL) sample = MAX_SAMPLE_VAL-1;
	//			if (sample < -MAX_SAMPLE_VAL) sample = -MAX_SAMPLE_VAL;
	//			
	//			outBuffer[2*i]   = (byte) (sample & 0xff);
	//			outBuffer[2*i+1] = (byte) (sample >> 8);
	//		}
	//	}

	/**
	 * Opens the specified WAV file and returns an AudioInputStream for reading from it
	 * @throws IOException something went wrong
	 * @throws UnsupportedAudioFileException something else went wrong, but it was your fault
	 * @param fn The file name
	 * @return the AudioInputStream
	 */
	private static AudioInputStream openWAV(String fn) throws IOException, UnsupportedAudioFileException {
		AudioInputStream stream;
		try {
			stream = AudioSystem.getAudioInputStream(new BufferedInputStream(new FileInputStream(fn)));
			if (!stream.getFormat().matches(REQUIRED_AUDIO_FORMAT))
				throw new UnsupportedAudioFileException();
		} catch (UnsupportedAudioFileException e) { // catches non-matching format and unopenable stream
			throw new UnsupportedAudioFileException(fn + " (Must be " + REQUIRED_AUDIO_FORMAT + ")");
		}
		return stream;
	}

	/**
	 * Generates a pre-computed array of cosine window coefficients, defined by
	 * 
	 * \[ W(n) = \sum_{i=0}^{n-1} (-1)^i a_i cos(\frac{2 \pi k n}{N-1} \]
	 * 
	 * 
	 * @param coefs the cosine coeffients a_1..a_n
	 * @return double[] the window coefficients
	 */
	protected static double[] generalizedCosineWindow(double[] coefs) {
		double[] window = new double[WINDOW_SIZE];
		for (int i = 0; i < WINDOW_SIZE; i++) {
			int sign = 1;
			for (int k = 0; k < coefs.length; k++) {
				window[i] += sign*coefs[k] * Math.cos(2*Math.PI*k*i/(WINDOW_SIZE-1));
				sign *= -1;
			}
			// scale by constant overlap sum, root to allow pre- AND post-windowing
			window[i] = Math.sqrt(HOP_FRACTION / GCW_COEFS[0] * window[i]);
			//window[i] = HOP_FRACTION / GCW_COEFS[0] * window[i];
		}
		return window;
	}

	/**
	 * Sets constants based on command line options
	 * @param opt the options from the command line
	 * @see Options
	 */
	protected static void setConstants(Options opt) {
		WINDOW_SIZE = opt.windowSize;
	    WINDOW_FUNC = generalizedCosineWindow(GCW_COEFS);
	    NUM_CHUNKS = (int) (1 / HOP_FRACTION);
	    CHUNK_SIZE = (int) (WINDOW_SIZE * HOP_FRACTION); 
	    BYTES_PER_CHUNK = CHUNK_SIZE * BYTES_PER_FRAME;
	    srcBuffer = new byte[BYTES_PER_CHUNK];
	    tgtBuffer = new byte[BYTES_PER_CHUNK];
	    outBuffer = new byte[BYTES_PER_CHUNK];
	    srcWindow = new double[WINDOW_SIZE];
	    tgtWindow = new double[WINDOW_SIZE];
	    outWindow = new double[WINDOW_SIZE];
	    srcChunks = new double[NUM_CHUNKS][CHUNK_SIZE];
	    tgtChunks = new double[NUM_CHUNKS][CHUNK_SIZE];
	    outChunks = new double[NUM_CHUNKS][CHUNK_SIZE];
	    realtime = opt.realtime;
	    timing = opt.timing;
	    debug = opt.debug;
	    maxLength = opt.length * 44165;
	    outFile = opt.outFile;
	    if(!(opt.interval.equals("none"))){
	      opt.beta = setInterval(opt.interval);
	    }
	    realtimeSrc = (opt.srcFile == null);
	    realtimeTgt = (opt.tgtFile == null && !opt.tpain && opt.beta==-1.0);
	    AutoTune.harmonicPeaks = opt.harmonicPeaks;
		FFT.init();
	}
	/**
	 * Given a string representation of an interval, set beta accordingly
	 * @param interval the interval to set beta to. Px is perfect x, Ax is augmented x, Dx is diminished x, Mx is major x, mx is minor x.
	 * @return double beta value
	 */
	protected static double setInterval(String interval){
		double beta = -1;
		switch(interval){
		case "P1":
			beta = 1;
			break;
		case "d2":
			beta = 1;
			break;
		case "m2": //minor second
			beta = Math.pow(2, 1./12);
			break;
		case "A1":
			beta = Math.pow(2, 1./12);
			break;
		case "M2": //major second
			beta = Math.pow(2, 2./12);
			break;
		case "d3": //major second
			beta = Math.pow(2, 2./12);
			break;
		case "m3": //minor third
			beta = Math.pow(2, 3./12);
			break;
		case "A2": //minor third
			beta = Math.pow(2, 3./12);
			break;
		case "M3": //Major third
			beta = Math.pow(2, 1./3);
			break;
		case "d4": //Major third
			beta = Math.pow(2, 4./12);
			break;
		case "P4":
			beta = Math.pow(2, 5./12);
			break;
		case "A3":
			beta = Math.pow(2, 5./12);
			break;
		case "A4":
			beta = Math.pow(2, 6./12);
			break;
		case "d5":
			beta = Math.pow(2, 6./12);
			break;
		case "Tritone":
			beta = Math.pow(2, 6./12);
			break;
		case "P5":
			beta = Math.pow(2, 7./12);
			break;
		case "d6":
			beta = Math.pow(2, 7./12);
			break;
		case "m6":
			beta = Math.pow(2, 8./12);
			break;
		case "A5":
			beta = Math.pow(2, 8./12);
			break;
		case "M6":
			beta = Math.pow(2, 9./12);
			break;
		case "d7":
			beta = Math.pow(2, 9./12);
			break;
		case "m7":
			beta = Math.pow(2, 10./12);
			break;
		case "A6":
			beta = Math.pow(2, 10./12);
			break;
		case "M7":
			beta = Math.pow(2, 11./12);
			break;
		case "d8":
			beta = Math.pow(2, 11./12);
			break;
		case "P8":
			beta = 2;
			break;
		case "A7":
			beta = 2;
			break;
		default:
			System.out.println("Invalid Interval, who knows what could happen now!");
		}
		return beta;
	}

	/**
	 * Sets the algorithm for the source and target pitch detectors if one is given from the command line
	 * 
	 * @param opt set of command line options
	 * @param src The pitch detector for the source file.
	 * @param tgt The pitch detector for the target file.
	 */
	protected static void setAlgorithms(Options opt, PitchDetector src, PitchDetector tgt){
		if(opt.algSrc != null){
			switch(opt.algSrc.toLowerCase()){
			case "fullautocorrelation":
				src.algorithm = Algorithm.FULL_AUTOCORRELATE;
				break;
			case "fftautocorrelation":
				src.algorithm = Algorithm.FFT_AUTOCORRELATE;
				break;
			case "cepstrum":
				src.algorithm = Algorithm.CEPSTRUM;
				break;
			case "hps":
				src.algorithm = Algorithm.HPS;
				break;
			case "amdf":
				src.algorithm = Algorithm.AMDF;
				break;
			case "biggestpeak":
				src.algorithm = Algorithm.BIGGEST_PEAK;
				break;
			case "firstpeak":
				src.algorithm = Algorithm.FIRST_PEAK;
				break;
			case "cephps":
			case "hpscep":
				src.algorithm = Algorithm.HPS_CEP;
				break;
			}
		}
		if(opt.algTgt != null){
			switch(opt.algTgt.toLowerCase()){
			case "fullautocorrelation":
				tgt.algorithm = Algorithm.FULL_AUTOCORRELATE;
				break;
			case "fftautocorrelation":
				tgt.algorithm = Algorithm.FFT_AUTOCORRELATE;
				break;
			case "cepstrum":
				tgt.algorithm = Algorithm.CEPSTRUM;
				break;
			case "hps":
				tgt.algorithm = Algorithm.HPS;
				break;
			case "amdf":
				tgt.algorithm = Algorithm.AMDF;
				break;
			case "biggestpeak":
				tgt.algorithm = Algorithm.BIGGEST_PEAK;
				break;
			case "firstpeak":
				tgt.algorithm = Algorithm.FIRST_PEAK;
				break;
			case "cephps":
			case "hpscep":
				tgt.algorithm = Algorithm.HPS_CEP;
				break;
			}
		}
	}

	/**
	 * Chops off the already-processed first chunk and appends a new final one
	 * @param chunks the chunks to queue
	 */
	private static void queueChunks(double[][] chunks) {
		// shift the chunks forward, so the ith is in the (i-1)st position
		for (int i = 1; i < NUM_CHUNKS; i++)
			chunks[i-1] = chunks[i];
		// initialize new final chunks to avoid feedback
		chunks[NUM_CHUNKS-1] = new double[CHUNK_SIZE];
	}
	
	/**
	 * Corrects output file length if necessary
	 */
	private static void correctFileLength() {
		try {
		      outStream.close();
		    } catch (IOException e1) {
		      if (debug) {
		    	  e1.printStackTrace();
		      }
		      destruct(1);
		    }
		    AudioInputStream inputStream;
		    AudioInputStream correctedStream;
		    File tempInFile;
		    File outputFile;
		    try {
		      tempInFile = new File("TEMP.wav");
		      inputStream = AudioSystem.getAudioInputStream(tempInFile);
		      correctedStream = new AudioInputStream(inputStream, inputStream.getFormat(), length);
		      outputFile = new File(outFile);
		      AudioSystem.write(correctedStream, AudioFileFormat.Type.WAVE, outputFile);
		      try { 
		        inputStream.close(); 
		        correctedStream.close(); 
		      } catch (Exception e) { 
		        if(debug){
		          e.printStackTrace();
		        }
		        destruct(1);
		      }
		      tempInFile.delete();
		    } catch (IOException | UnsupportedAudioFileException e) {
		      System.err.println("Error opening file " + e.getMessage());
		      if(debug){
		        e.printStackTrace();
		      }
		      destruct(1);
		    }
	} 

	/**
	 * This method runs the entire program.<p>
	 * 1) set up: set constants, initialize streams, create objects.<p>
	 * 2) while loop: Read - Window - Pitch detect - Vocode - Combine - Unwindow - Write
	 * @param opt command line options
	 */
	public static void run(Options opt) {
		double start = System.currentTimeMillis();
		setConstants(opt);

		double windowNum = 0;
		double numSeconds = 0;

		// Check some necessary conditions about our constants
		assert (Math.log(WINDOW_SIZE) / Math.log(2)) % 1 == 0 : 
			"Window size not a power of two!";
		assert 0 < HOP_FRACTION && HOP_FRACTION <= 1 :
			"Invalid hop fraction!";
		assert WINDOW_SIZE == NUM_CHUNKS * CHUNK_SIZE :
			"Hop fraction does not divide window into equally-sized chunks!";
		assert NUM_CHUNKS >= GCW_COEFS.length :
			"Not enough window overlap to eliminate reconstruction modulation!";
		assert java.util.stream.DoubleStream.of(GCW_COEFS).sum() == 1 :
			"Window function coefs must sum to 1 for proper reconstruction!";

		// Set up I/O
		/*if (!realtime)
			initOfflineStreams(opt);
		else
			initRealtimeStreams(opt);*/
		initStreams(opt);


		PitchDetector srcPitchDetector, tgtPitchDetector = null;
		srcPitchDetector = new PitchDetector();
		srcPitchDetector.isVoiceFile = !opt.nvs;
		if (!realtimeTgt){
			tgtPitchDetector = new PitchDetector();
		}
		setAlgorithms(opt, srcPitchDetector, tgtPitchDetector);
		double initTime = System.currentTimeMillis() - start;

		// Each pass of this loop does the following:
		//  1. reads one chunk's worth of data from input(s)
		//  2. processes a full window of audio (pitch detection + phase vocoding)
		//  3. writes one chunk of output
		int srcBytesRead = 0, numReads = 0, tgtBytesRead = 0;

		if(tgtStream != null){
			tgtStream.mark(Integer.MAX_VALUE);
		}
		if(realtimeTgt){
			srcStream.mark(Integer.MAX_VALUE);
		}
		isRunning = true;
		if(realtime || realtimeSrc || realtimeTgt){
			System.out.println("Ready to go!");
			System.out.println("Press Enter to start autotuning and again to stop.");
			in = new Scanner(System.in);
			in.nextLine();
			StopThread t = new StopThread();
			t.start();
			if(realtimeSrc){
				srcDataLine.start();
			}
		}
		
		
		ArrayList<Double> src_pitches = new ArrayList<Double>();
		ArrayList<Double> tgt_pitches = new ArrayList<Double>();
		ArrayList<Double> out_pitches = new ArrayList<Double>();
		
		while (isRunning) {
			double windowStart = System.currentTimeMillis();
			// Try to read a chunk from both src and tgt
			try {
				srcBytesRead = srcStream.read(srcBuffer);
				if (!realtimeTgt && opt.beta==-1 && !opt.tpain) 
					tgtBytesRead = tgtStream.read(tgtBuffer);
			} catch (IOException e) { // should never happen...
				if(debug){
					e.printStackTrace();
				}
				destruct(1);
			}
			if(realtimeTgt && srcBytesRead < 0){
				try{
					srcStream.reset();
					srcBytesRead = srcStream.read(srcBuffer);
				}
				catch(Exception e){
					if(debug){
						e.printStackTrace();
					}				
				}
			}
			// If src ran out of audio, then we're done
			if (srcBytesRead < 0)
				break;
			else if(!realtimeTgt && tgtBytesRead < 0){
				try {
					tgtStream.reset();
					tgtBytesRead = tgtStream.read(tgtBuffer);
				} catch (IOException e) {
					if(debug){
						e.printStackTrace();
					}				
					destruct(1);
				}
			}


			// Zero out any bytes past the end of the file
			for (int i = srcBytesRead; i < BYTES_PER_CHUNK; i++)
				srcBuffer[i] = 0;

			// Prepare the chunks to receive more data
			queueChunks(srcChunks);
			if (!realtimeTgt) queueChunks(tgtChunks);
			queueChunks(outChunks);

			// Read samples into the new final chunks
			readChunkFromBuffer(srcBuffer, srcChunks[NUM_CHUNKS-1]);
			if (!realtimeTgt) readChunkFromBuffer(tgtBuffer, tgtChunks[NUM_CHUNKS-1]);

			double[] srcUnwindowed, tgtUnwindowed = null;
			srcUnwindowed = new double[NUM_CHUNKS * CHUNK_SIZE];
			if (!realtimeTgt) tgtUnwindowed = new double[NUM_CHUNKS * CHUNK_SIZE];

			// Only process if we've read a full window's worth of chunks
			if (++numReads < NUM_CHUNKS)
				continue;

			// Assemble chunks into window
			for (int i = 0; i < NUM_CHUNKS; i++) {
				for (int j = 0; j < CHUNK_SIZE; j++) {
					int n = i*CHUNK_SIZE + j;
					srcUnwindowed[n] = srcChunks[i][j];
					srcWindow[n] = WINDOW_FUNC[n]*srcChunks[i][j];
					if (!realtimeTgt) {
						tgtUnwindowed[n] = tgtChunks[i][j];
						tgtWindow[n] = WINDOW_FUNC[n]*tgtChunks[i][j];
					}
				}
			}
			windowTime += System.currentTimeMillis() - windowStart;
			double peakStart = System.currentTimeMillis();
			// Pre-perform FFTs and peak analysis so the work is done only once
			SignalSpectrum srcSignal, tgtSignal = null;
			srcSignal = new SignalSpectrum(srcWindow, !opt.nvs);
			srcSignal.setUnwindowedSignal(srcUnwindowed);
			SignalSpectrumInitTime += System.currentTimeMillis() - peakStart;
			if (!realtimeTgt) {
				tgtSignal = new SignalSpectrum(tgtWindow);
				tgtSignal.setUnwindowedSignal(tgtUnwindowed);
			}

			double beta;

			if(opt.beta != -1.0){
				PitchThread srcThread = new PitchThread(srcSignal, srcPitchDetector);
				srcThread.start();
				try {
					srcThread.join();
				} catch (InterruptedException e) {
					if(debug){
						e.printStackTrace();
					}
					destruct(0);
				}
				srcSignal.fundamental = srcThread.pitch;
				beta = opt.beta;
				
				
			}
			else if(opt.multiNote && realtimeTgt && !opt.tpain){
				double srcPitch;
				PitchThread srcThread = new PitchThread(srcSignal, srcPitchDetector);
				srcThread.run();
				srcPitch = srcThread.pitch;
				srcSignal.fundamental = srcPitch;
				ArrayList<Double> tgtPitches = tgtMidiListener.getPitches();
				beta = 1;
				
				if(opt.graph == true){
					src_pitches.add(srcSignal.fundamental);
				}
				
				int numPitches = tgtPitches.size();
				if(numPitches == 0){
					outWindow = PhaseVocoder.phaseVocode(srcSignal, beta);
					//Break output window into chunks and update existing output
					for (int n = 0; n < WINDOW_SIZE; n++) {
						int i = n / CHUNK_SIZE;
						int j = n % CHUNK_SIZE;
						outChunks[i][j] += outWindow[n]*WINDOW_FUNC[n]; // re-window output
					}
				}
				for (double tgtPitch : tgtPitches){
					beta = (tgtPitch == 0 || srcPitch == 0) ? 1 : tgtPitch/srcPitch;
					outWindow = PhaseVocoder.phaseVocode(srcSignal, beta);
					for (int n = 0; n < WINDOW_SIZE; n++) {
						int i = n / CHUNK_SIZE;
						int j = n % CHUNK_SIZE;
						outChunks[i][j] += (1. / numPitches + .1) * outWindow[n] * WINDOW_FUNC[n]; // re-window output. The .1 is a made up number to make it not get quieter as more notes are added.
					}
				}
			}
			else{
				// Compute beta from src and tgt pitches
				double srcPitch, tgtPitch;

				PitchThread srcThread = new PitchThread(srcSignal, srcPitchDetector);
				srcThread.start();
				tgtPitch = 0;
				if (!realtimeTgt){
					PitchThread tgtThread = new PitchThread(tgtSignal, tgtPitchDetector);
					tgtThread.run();
					tgtPitch = tgtThread.pitch;
				}

				try {
					srcThread.join();
				} catch (InterruptedException e) {
					if(debug){
						e.printStackTrace();
					}
					destruct(1);
				}
				srcPitch = srcThread.pitch;
				srcSignal.fundamental = srcPitch;
				if(opt.tpain){
					double k = 12 * Math.log(srcPitch / 440) / Math.log(2) + 49; //Formula for piano key number from Wikipedia
					k = Math.round(k); //find the nearest key
					if(k % 2 == 1){
						k += 1; //nearest even key
					}
					double freq = Math.pow(2, (k-49) / 12) * 440; //convert back to hertz
					beta = srcPitch / freq;
					// Phase vocode away!
					outWindow = PhaseVocoder.phaseVocode(srcSignal, beta);
					if(opt.graph == true){
						src_pitches.add(srcSignal.fundamental);
						tgt_pitches.add(srcSignal.fundamental*beta);
					}
					// Break output window into chunks and update existing output
					for (int n = 0; n < WINDOW_SIZE; n++) {
						int i = n / CHUNK_SIZE;
						int j = n % CHUNK_SIZE;
						double hScaleFac = (opt.harmonize) ? 0.5 : 1.0;
						outChunks[i][j] += hScaleFac*outWindow[n]*WINDOW_FUNC[n]; // re-window output
						if (opt.harmonize)
							outChunks[i][j] += hScaleFac*srcWindow[n];
					}
				}
				else{
					beta = (tgtPitch == 0 || srcPitch == 0) ? 1 : tgtPitch/srcPitch; // either pitchless => no shift
				}
			}
			if(!opt.multiNote || !(realtimeTgt)){
				// Phase vocode away!
				outWindow = PhaseVocoder.phaseVocode(srcSignal, beta);
				if(opt.graph == true){
					src_pitches.add(srcSignal.fundamental);
					tgt_pitches.add(srcSignal.fundamental*beta);
				}
				// Break output window into chunks and update existing output
				for (int n = 0; n < WINDOW_SIZE; n++) {
					int i = n / CHUNK_SIZE;
					int j = n % CHUNK_SIZE;
					double hScaleFac = (opt.harmonize) ? 0.5 : 1.0;
					outChunks[i][j] += hScaleFac*outWindow[n]*WINDOW_FUNC[n]; // re-window output
					if (opt.harmonize)
						outChunks[i][j] += hScaleFac*srcWindow[n];
				}
			}

			// The first chunk is now complete; write it to output!
			writeChunkToOut(outChunks[0]);
			windowNum++;
			if(AutoTune.SAMPLE_RATE / AutoTune.WINDOW_SIZE / AutoTune.HOP_FRACTION < windowNum){
				windowNum = 0;
				numSeconds++;
				System.out.println("Just finished second " + numSeconds + " of audio");
			}
			
			
		}

		// When end of input is reached, flush the remaining chunks to the output stream
		// (skip the first one because it will have been written at the end of the last iteration)
		for (int i = 1; i < NUM_CHUNKS; i++)
			writeChunkToOut(outChunks[i]);

		if(timing){
			System.out.println("Total time spent on detecting pitch in source: " + (srcPitchDetector.totalTimeSpent / 1000));
			System.out.println("Total time spent in phase vocoder: " + (PhaseVocoder.time / 1000));
			System.out.println("Total time spent initializing SignalSpectrums: " + (SignalSpectrumInitTime)/1000);
			System.out.println("Total time spent on Cepstrum in SigSpec: " + (SignalSpectrum.cepstrumTiming)/1000);
			System.out.println("Total time spent on interpolating in SigSpec: " + (SignalSpectrum.interpolateTiming)/1000);
			System.out.println("Total time spent doing peaks in SigSpec: " + (SignalSpectrum.peaksTiming)/1000);
			System.out.println("Total time spent doing FFT in SigSpec: " + (SignalSpectrum.FFTTiming)/1000);
			System.out.println("Time spent doing trig in SigSpec: " + SignalSpectrum.trigTiming/1000);
			System.out.println("Total time spent on windowing and related activities: " + windowTime/1000);
			System.out.println("Time spent initializing AutoTune things: " + initTime/1000);
			System.out.println("Total time: " + ((System.currentTimeMillis() - start) / 1000));
		}
		
		if (realtimeTgt || realtimeSrc) {
			correctFileLength();
		}
		
		if(opt.graph==true){
			double frameLength = srcStream.getFrameLength();
			double frameRate = srcStream.getFormat().getFrameRate();
			double[] time_doubles = new double[src_pitches.size()];
			double[] src_pitch_doubles = new double[src_pitches.size()];
			double[] tgt_pitch_doubles = new double[tgt_pitches.size()];
			for (int i = 0; i < src_pitches.size(); i++){
				src_pitch_doubles[i] = src_pitches.get(i);
			}
			for (int i = 0; i < tgt_pitches.size(); i++){
				tgt_pitch_doubles[i] = tgt_pitches.get(i);
			}
			for (int i = 0; i < time_doubles.length; i++){
				time_doubles[i] = i*1.0/time_doubles.length*frameLength/frameRate;
			}
			Thread vis = new Thread() {
	            @Override
	            
	            public void run() {
	            	Visualizer v = new Visualizer("Pitches","Time (sec)","Pitch (Hz)");
	            	v.addSeries("Source Pitch",time_doubles,src_pitch_doubles);
	            	v.addSeries("Tgt Pitch",time_doubles,tgt_pitch_doubles);
	            	v.launch(Visualizer.class);
	            }
	        };
	        vis.start();
	        try {
				vis.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

		
		// Success!
		destruct(0);
	}

	/**
	 * A thread which controls stopping real time autotuning <p>
	 * Press enter to end the program
	 */
	private static class StopThread extends Thread{
		/**
		 * Stop the main thread when a new line is entered on the terminal
		 */
		@Override
		public void run(){
			in.nextLine();
			isRunning = false;
			in.close();
		}
	}
}
