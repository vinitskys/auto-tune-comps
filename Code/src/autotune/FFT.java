

package autotune;
/**
 * Standard forward and inverse FFT implementations. Normalization factor is split evenly between forward and inverse calls.
 * <p>
 * Application is done in SignalSpectrum, the results of which are then shared by PitchDetector and PhaseVocoder.
 * @see SignalSpectrum
 * @see PhaseVocoder
 * @see PitchDetector
 */
public class FFT {
	
	private static boolean  isSetup = false;
	
	private static int      size; // the FFT size (=AutoTune.WINDOW_SIZE)
	private static double   unf;  // the unitary normalization factor; makes ifft(fft(x)) = x
	private static int[]    BREV; // lookup table for bit-reversals used in butterfly swapping
	private static double[] TRIG; // lookup table for sin(x) evaluated on [0, 3pi/2)
	
	private static final double sin_t(int x) { return TRIG[x]; }
	private static final double cos_t(int x) { return TRIG[x + size/2]; }

	/**
	 * Sets up constants and lookup tables. MUST BE CALLED PRIOR TO ANY FFT CALLS.
	 */
	public static void init() {
		size = AutoTune.WINDOW_SIZE;
		unf = Math.pow(size, -0.5);
		
		// check that we have a power of 2 to work with
		int base = (int) Math.round(Math.log(size) / Math.log(2));
		if (Math.pow(2, base) != size)
			throw new RuntimeException("I like powers of 2 and ONLY powers of 2!!!");
		
		// populate the bit-rev lookup table
		BREV = new int[size];
		for (int i = 0; i < size; i++)
			BREV[i] = brev(i, base);
		
		// populate the trig lookup table
		int trigSize = (int) (1.5 * size);
		TRIG = new double[trigSize];
		for (int i = 0; i < trigSize; i++)
			TRIG[i] = Math.sin(Math.PI*i/size);
		
		// good to go!
		isSetup = true;
	}

	/**
	 * Reverses the n least significant bits of x.
	 * 
	 * @param x The source index
	 * @param n The number of binary digits to work with
	 * @return The bit-reversed target index
	 */
	private static int brev(int x, int n) {
		int y = 0;
		while (n-- > 0) {
			y <<= 1;
			y |= (x & 1);
			x >>= 1;
		}
		return y;
	}

	/**
	 * Actual FFT implementation: computes in place
	 * @param real Real part
	 * @param imag Imaginary part
	 */
	public static void fft(double[] real, double[] imag) {
		assert isSetup;
		assert real.length == imag.length;

		// butterfly swaps
		for (int x = 0; x < size; x++) {
			int y = BREV[x];
			double swap;
			if (x < y) { // prevent double swap x <=> y and then y <=> x later
				swap = real[x]; real[x] = real[y]; real[y] = swap;
				swap = imag[x]; imag[x] = imag[y]; imag[y] = swap;
			}
			// scale while we're at it
			real[x] *= unf;
			imag[x] *= unf;
		}

		int d = 1; // step between each sub-iteration (current sub-FFT's size)
		while (d < size) {
			int f = size/d;
			int i = 0; // master index, current position inside full array
			while (i < size) {
				int k = 0; // relative offset within current sub-iteration
				while (k < d) {
					int j = i + d;

					double tw_cos = cos_t(f*k);
					double tw_sin = sin_t(f*k);
					
					double A_re, A_im, B_re, B_im;

					A_re = real[i];
					A_im = imag[i];
					B_re = real[j]*tw_cos + imag[j]*tw_sin;
					B_im = imag[j]*tw_cos - real[j]*tw_sin;

					// complex multiplication
					real[i] = A_re + B_re;
					imag[i] = A_im + B_im;
					real[j] = A_re - B_re;
					imag[j] = A_im - B_im;

					k++; // step ahead in the sub-iteration
					i++; // update master index to reflect this
				}
				i += d; // move master index ahead to next sub-iteration
			}
			// bump up to next step size
			d *= 2;
		}
	}

	/**
	 * Inverse FFT: also in-place
	 * @param real Real part
	 * @param imag Imaginary part
	 */
	public static void ifft(double[] real, double[] imag) {
		assert isSetup;
		fft(imag, real);
	}
}
